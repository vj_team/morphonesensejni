#include <jni.h>
#include <stdio.h>
#include <android/log.h>

#include <utilities.h>
#include <BluetoothSenseReceiver.h>

JNIEXPORT jobject JNICALL Java_org_morphone_sense_bluetooth_BluetoothSenseReceiver_getBondedDevicesNative
  (JNIEnv *pEnv, jobject pObj) {
	jclass BluetoothSenseReceiverCls = (*pEnv)->GetObjectClass(pEnv, pObj);

	jfieldID bluetoothAdapterID = (*pEnv)->GetFieldID(pEnv, BluetoothSenseReceiverCls, "bluetoothAdapter", "Landroid/bluetooth/BluetoothAdapter;");
	jobject bluetoothAdapter = (*pEnv)->GetObjectField(pEnv, pObj, bluetoothAdapterID);

	jclass BluetoothAdapterCls = (*pEnv)->GetObjectClass(pEnv, bluetoothAdapter);

	jmethodID baGetBondedDevicesMID = (*pEnv)->GetMethodID(pEnv, BluetoothAdapterCls, "getBondedDevices", "()Ljava/util/Set;");
	jobject ret = (*pEnv)->CallObjectMethod(pEnv, bluetoothAdapter, baGetBondedDevicesMID);
	if((*pEnv)->ExceptionOccurred(pEnv)) {
		throwBluetoothSenseException(pEnv, (*pEnv)->NewStringUTF(pEnv,"Error while getting BondedDevices"));
	}	

	return ret;
}

JNIEXPORT jint JNICALL Java_org_morphone_sense_bluetooth_BluetoothSenseReceiver_getStateNative
  (JNIEnv *pEnv, jobject pObj) {
	jclass BluetoothSenseReceiverCls = (*pEnv)->GetObjectClass(pEnv, pObj);

	jfieldID currentStateID = (*pEnv)->GetFieldID(pEnv, BluetoothSenseReceiverCls, "currentState", "I");
	jint currentState = (*pEnv)->GetIntField(pEnv, pObj, currentStateID);
	if((*pEnv)->ExceptionOccurred(pEnv)) {
		throwBluetoothSenseException(pEnv, (*pEnv)->NewStringUTF(pEnv,"Error while getting State"));
	}
	return currentState;
}

JNIEXPORT jboolean JNICALL Java_org_morphone_sense_bluetooth_BluetoothSenseReceiver_isActiveNative
  (JNIEnv *pEnv, jobject pObj) {
	jclass BluetoothSenseReceiverCls = (*pEnv)->GetObjectClass(pEnv, pObj);

	jfieldID currentStateID = (*pEnv)->GetFieldID(pEnv, BluetoothSenseReceiverCls, "currentState", "I");
	jint currentState = (*pEnv)->GetIntField(pEnv, pObj, currentStateID);

	jclass BluetoothAdapterCls = (*pEnv)->FindClass(pEnv, "android/bluetooth/BluetoothAdapter");
	jfieldID STATE_OFFID = (*pEnv)->GetStaticFieldID(pEnv, BluetoothAdapterCls, "STATE_OFF", "I");
	int STATE_OFF = (*pEnv)->GetStaticIntField(pEnv, BluetoothAdapterCls, STATE_OFFID);
	jfieldID STATE_TURNING_OFFID = (*pEnv)->GetStaticFieldID(pEnv, BluetoothAdapterCls, "STATE_TURNING_OFF", "I");
	int STATE_TURNING_OFF = (*pEnv)->GetStaticIntField(pEnv, BluetoothAdapterCls, STATE_TURNING_OFFID);
	if((*pEnv)->ExceptionOccurred(pEnv)) {
		throwBluetoothSenseException(pEnv, (*pEnv)->NewStringUTF(pEnv,"Error while getting BondedDevices"));
	}
	if (currentState!=STATE_OFF && currentState!=STATE_TURNING_OFF)
		return 1;
	else
		return 0;
}

JNIEXPORT void JNICALL Java_org_morphone_sense_bluetooth_BluetoothSenseReceiver_onReceiveNative
  (JNIEnv *pEnv, jobject pObj, jobject pContext, jobject pIntent) {
	jclass BluetoothSenseReceiverCls = (*pEnv)->GetObjectClass(pEnv, pObj);

	jfieldID currentStateID = (*pEnv)->GetFieldID(pEnv, BluetoothSenseReceiverCls, "currentState", "I");
	jint currentState = (*pEnv)->GetIntField(pEnv, pObj, currentStateID);
	jfieldID previousStateID = (*pEnv)->GetFieldID(pEnv, BluetoothSenseReceiverCls, "previousState", "I");
	jint previousState = (*pEnv)->GetIntField(pEnv, pObj, previousStateID);

	int cState = -1;
	int pState = -1;

	jclass intentCls = (*pEnv)->GetObjectClass(pEnv, pIntent);
	jmethodID getIntExtraMID = (*pEnv)->GetMethodID(pEnv, intentCls, "getIntExtra", "(Ljava/lang/String;I)I");

	jclass BluetoothAdapterCls = (*pEnv)->FindClass(pEnv, "android/bluetooth/BluetoothAdapter");
	jfieldID baEXTRA_STATEID = (*pEnv)->GetStaticFieldID(pEnv, BluetoothAdapterCls, "EXTRA_STATE", "Ljava/lang/String;");
	jstring baEXTRA_STATE = (jstring)(*pEnv)->GetStaticObjectField(pEnv, BluetoothAdapterCls, baEXTRA_STATEID);
	jfieldID baEXTRA_PREVIOUS_STATEID = (*pEnv)->GetStaticFieldID(pEnv, BluetoothAdapterCls, "EXTRA_PREVIOUS_STATE", "Ljava/lang/String;");
	jstring baEXTRA_PREVIOUS_STATE = (jstring)(*pEnv)->GetStaticObjectField(pEnv, BluetoothAdapterCls, baEXTRA_PREVIOUS_STATEID);

	cState = (*pEnv)->CallIntMethod(pEnv, pIntent, getIntExtraMID, baEXTRA_STATE, -1);
	pState = (*pEnv)->CallIntMethod(pEnv, pIntent, getIntExtraMID, baEXTRA_PREVIOUS_STATE, -1);

	(*pEnv)->SetIntField(pEnv, pObj, currentStateID, cState);
	(*pEnv)->SetIntField(pEnv, pObj, previousStateID, pState);

}

void throwBluetoothSenseException(JNIEnv *pEnv, jstring eMsg) {
	jclass BluetoothSenseExceptionCls = (*pEnv)->FindClass(pEnv, "org/morphone/sense/bluetooth/BluetoothSenseException");
	if (BluetoothSenseExceptionCls!=NULL) 
		(*pEnv)->ThrowNew(pEnv, BluetoothSenseExceptionCls, eMsg);
	(*pEnv)->DeleteLocalRef(pEnv, BluetoothSenseExceptionCls);
}

