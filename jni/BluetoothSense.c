#include <jni.h>
#include <stdio.h>
#include <android/log.h>

#include <utilities.h>
#include <BluetoothSense.h>

JNIEXPORT jobject JNICALL Java_org_morphone_sense_bluetooth_BluetoothSense_getBondedDevicesNative
  (JNIEnv *pEnv, jobject pObj) {
	jclass BluetoothSenseCls = (*pEnv)->GetObjectClass(pEnv, pObj);

	jfieldID bluetoothAdapterID = (*pEnv)->GetFieldID(pEnv, BluetoothSenseCls, "bluetoothAdapter", "Landroid/bluetooth/BluetoothAdapter;");
	jobject bluetoothAdapter = (*pEnv)->GetObjectField(pEnv, pObj, bluetoothAdapterID);

	jclass BluetoothAdapterCls = (*pEnv)->GetObjectClass(pEnv, bluetoothAdapter);

	jmethodID baGetBondedDevicesMID = (*pEnv)->GetMethodID(pEnv, BluetoothAdapterCls, "getBondedDevices", "()Ljava/util/Set;");
	jobject ret = (*pEnv)->CallObjectMethod(pEnv, bluetoothAdapter, baGetBondedDevicesMID);
	if((*pEnv)->ExceptionOccurred(pEnv)) {
		throwBluetoothSenseException(pEnv, (*pEnv)->NewStringUTF(pEnv,"Error while getting BondedDevices"));
	}	

	return ret;
}

JNIEXPORT jint JNICALL Java_org_morphone_sense_bluetooth_BluetoothSense_getStateNative
  (JNIEnv *pEnv, jobject pObj) {
	jclass BluetoothSenseCls = (*pEnv)->GetObjectClass(pEnv, pObj);

	jfieldID bluetoothAdapterID = (*pEnv)->GetFieldID(pEnv, BluetoothSenseCls, "bluetoothAdapter", "Landroid/bluetooth/BluetoothAdapter;");
	jobject bluetoothAdapter = (*pEnv)->GetObjectField(pEnv, pObj, bluetoothAdapterID);

	jclass BluetoothAdapterCls = (*pEnv)->GetObjectClass(pEnv, bluetoothAdapter);

	jmethodID baGetStateMID = (*pEnv)->GetMethodID(pEnv, BluetoothAdapterCls, "getState", "()I");
	int baGetStateRet = (*pEnv)->CallIntMethod(pEnv, bluetoothAdapter, baGetStateMID);
	if((*pEnv)->ExceptionOccurred(pEnv)) {
		throwBluetoothSenseException(pEnv, (*pEnv)->NewStringUTF(pEnv,"Error while getting State"));
	}	
	return baGetStateRet;
}

JNIEXPORT jboolean JNICALL Java_org_morphone_sense_bluetooth_BluetoothSense_isActiveNative
  (JNIEnv *pEnv, jobject pObj) {
	jclass BluetoothSenseCls = (*pEnv)->GetObjectClass(pEnv, pObj);

	jfieldID bluetoothAdapterID = (*pEnv)->GetFieldID(pEnv, BluetoothSenseCls, "bluetoothAdapter", "Landroid/bluetooth/BluetoothAdapter;");
	jobject bluetoothAdapter = (*pEnv)->GetObjectField(pEnv, pObj, bluetoothAdapterID);

	jclass BluetoothAdapterCls = (*pEnv)->GetObjectClass(pEnv, bluetoothAdapter);

	jmethodID baGetStateMID = (*pEnv)->GetMethodID(pEnv, BluetoothAdapterCls, "getState", "()I");
	int getState = (*pEnv)->CallIntMethod(pEnv, bluetoothAdapter, baGetStateMID);

	jfieldID STATE_OFFID = (*pEnv)->GetStaticFieldID(pEnv, BluetoothAdapterCls, "STATE_OFF", "I");
	int STATE_OFF = (*pEnv)->GetStaticIntField(pEnv, BluetoothAdapterCls, STATE_OFFID);
	jfieldID STATE_TURNING_OFFID = (*pEnv)->GetStaticFieldID(pEnv, BluetoothAdapterCls, "STATE_TURNING_OFF", "I");
	int STATE_TURNING_OFF = (*pEnv)->GetStaticIntField(pEnv, BluetoothAdapterCls, STATE_TURNING_OFFID);
	if((*pEnv)->ExceptionOccurred(pEnv)) {
		throwBluetoothSenseException(pEnv, (*pEnv)->NewStringUTF(pEnv,"Error while getting BondedDevices"));
	}	

	if (getState!=STATE_OFF && getState!=STATE_TURNING_OFF)
		return 1;
	else
		return 0;
}

void throwBluetoothSenseException(JNIEnv *pEnv, jstring eMsg) {
	jclass BluetoothSenseExceptionCls = (*pEnv)->FindClass(pEnv, "org/morphone/sense/bluetooth/BluetoothSenseException");
	if (BluetoothSenseExceptionCls!=NULL) 
		(*pEnv)->ThrowNew(pEnv, BluetoothSenseExceptionCls, eMsg);
	(*pEnv)->DeleteLocalRef(pEnv, BluetoothSenseExceptionCls);
}
