void printClassName(JNIEnv *pEnv, jobject pObj, jclass cls){
	// First get the class object
	jmethodID mid = (*pEnv)->GetMethodID(pEnv, cls, "getClass", "()Ljava/lang/Class;");
	jobject clsObj = (*pEnv)->CallObjectMethod(pEnv, pObj, mid);
	// Now get the class object's class descriptor
	cls = (*pEnv)->GetObjectClass(pEnv, clsObj);
	// Find the getName() method on the class object
	mid = (*pEnv)->GetMethodID(pEnv, cls, "getName", "()Ljava/lang/String;");
	// Call the getName() to get a jstring object back
	jstring strObj = (jstring)(*pEnv)->CallObjectMethod(pEnv, clsObj, mid);
	// Now get the c string from the java jstring object
	const char* str = (*pEnv)->GetStringUTFChars(pEnv, strObj, NULL);
	// Print the class name
	__android_log_print(ANDROID_LOG_INFO, "Morphone", "Class: %s", str);
	// Release the memory pinned char array
	(*pEnv)->ReleaseStringUTFChars(pEnv, strObj, str);
}

void reachHere() {
	__android_log_print(ANDROID_LOG_INFO, "Morphone", "Reach this point");
}
