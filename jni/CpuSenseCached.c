#include <jni.h>
#include <stdio.h>
#include <android/log.h>

#include <utilities.h>
#include <CpuSenseCached.h>

JNIEXPORT jint JNICALL Java_org_morphone_sense_cpu_CpuSenseCached_getNumberOfCPUsNative
  (JNIEnv *pEnv, jobject pObj) {
    jclass CpuSenseCachedCls = (*pEnv)->GetObjectClass(pEnv, pObj);
    jfieldID cpu_numberID = (*pEnv)->GetFieldID(pEnv, CpuSenseCachedCls, "cpu_number", "I");
    int cpu_number = (*pEnv)->GetIntField(pEnv, pObj, cpu_numberID);
    if (cpu_number >= 0)
      return cpu_number;
    else
      throwCpuSenseException(pEnv, (*pEnv)->NewStringUTF(pEnv, "Error while getting cpu number"));
}

JNIEXPORT jint JNICALL Java_org_morphone_sense_cpu_CpuSenseCached_getCpuCurrentFrequencyNative
  (JNIEnv *pEnv, jobject pObj, jint cpu_index) {
    char path[256];
    sprintf(path, "/sys/devices/system/cpu/cpu%d/cpufreq/scaling_cur_freq", cpu_index);
    // strcpy(path, "/sys/devices/system/cpu/cpu");
    // strcat(path, cpu_index);
    // strcat(path, "/cpufreq/scaling_cur_freq");
    int ret = getValue(pEnv, path);
    if((*pEnv)->ExceptionOccurred(pEnv)) {
      return -1;
    } else
      return ret;
}

JNIEXPORT jint JNICALL Java_org_morphone_sense_cpu_CpuSenseCached_getCpuMaxScalingNative
  (JNIEnv *pEnv, jobject pObj, jint cpu_index) {
    jclass CpuSenseCachedCls = (*pEnv)->GetObjectClass(pEnv, pObj);
    jfieldID cpu_freq_scaling_maxID = (*pEnv)->GetFieldID(pEnv, CpuSenseCachedCls, "cpu_freq_scaling_max", "[I");
    jobject cpu_freq_scaling_maxObj = (*pEnv)->GetObjectField(pEnv, pObj, cpu_freq_scaling_maxID);
    jintArray *cpu_freq_scaling_maxArr = (jintArray*)(&cpu_freq_scaling_maxObj);
    jsize len = (*pEnv)->GetArrayLength(pEnv, *cpu_freq_scaling_maxArr);

    int value = 0;
    if (cpu_index<len) {
      jint *cpu_freq_scaling_max = (*pEnv)->GetIntArrayElements(pEnv, *cpu_freq_scaling_maxArr, 0);
      value = cpu_freq_scaling_max[cpu_index];
      if (value!=-1)
        return value;
      else
        throwCpuSenseException(pEnv, (*pEnv)->NewStringUTF(pEnv, "Error while getting cpu_freq_scaling_max, value = -1."));
    } else
      throwCpuSenseException(pEnv, (*pEnv)->NewStringUTF(pEnv, "Error while getting cpu_freq_scaling_max, cpu_index out of range."));
}

JNIEXPORT jint JNICALL Java_org_morphone_sense_cpu_CpuSenseCached_getCpuMinScalingNative
  (JNIEnv *pEnv, jobject pObj, jint cpu_index) {
    jclass CpuSenseCachedCls = (*pEnv)->GetObjectClass(pEnv, pObj);
    jfieldID cpu_freq_scaling_minID = (*pEnv)->GetFieldID(pEnv, CpuSenseCachedCls, "cpu_freq_scaling_min", "[I");
    jobject cpu_freq_scaling_minObj = (*pEnv)->GetObjectField(pEnv, pObj, cpu_freq_scaling_minID);
    jintArray *cpu_freq_scaling_minArr = (jintArray*)(&cpu_freq_scaling_minObj);
    jsize len = (*pEnv)->GetArrayLength(pEnv, *cpu_freq_scaling_minArr);

    int value = 0;
    if (cpu_index<len) {
      jint *cpu_freq_scaling_min = (*pEnv)->GetIntArrayElements(pEnv, *cpu_freq_scaling_minArr, 0);
      value = cpu_freq_scaling_min[cpu_index];
      if (value!=-1)
        return value;
      else
        throwCpuSenseException(pEnv, (*pEnv)->NewStringUTF(pEnv, "Error while getting cpu_freq_scaling_min, value = -1."));
    } else
      throwCpuSenseException(pEnv, (*pEnv)->NewStringUTF(pEnv, "Error while getting cpu_freq_scaling_min, cpu_index out of range."));
}

JNIEXPORT jint JNICALL Java_org_morphone_sense_cpu_CpuSenseCached_getCpuMaxFrequencyNative
  (JNIEnv *pEnv, jobject pObj, jint cpu_index) {
    jclass CpuSenseCachedCls = (*pEnv)->GetObjectClass(pEnv, pObj);
    jfieldID cpu_freq_maxID = (*pEnv)->GetFieldID(pEnv, CpuSenseCachedCls, "cpu_freq_max", "[I");
    jobject cpu_freq_maxObj = (*pEnv)->GetObjectField(pEnv, pObj, cpu_freq_maxID);
    jintArray *cpu_freq_maxArr = (jintArray*)(&cpu_freq_maxObj);
    jsize len = (*pEnv)->GetArrayLength(pEnv, *cpu_freq_maxArr);

    int value = 0;
    if (cpu_index<len) {
      jint *cpu_freq_max = (*pEnv)->GetIntArrayElements(pEnv, *cpu_freq_maxArr, 0);
      value = cpu_freq_max[cpu_index];
      if (value!=-1)
        return value;
      else
        throwCpuSenseException(pEnv, (*pEnv)->NewStringUTF(pEnv, "Error while getting cpu_freq_max, value = -1."));
    } else
      throwCpuSenseException(pEnv, (*pEnv)->NewStringUTF(pEnv, "Error while getting cpu_freq_max, cpu_index out of range."));
}

JNIEXPORT jint JNICALL Java_org_morphone_sense_cpu_CpuSenseCached_getCpuMinFrequencyNative
  (JNIEnv *pEnv, jobject pObj, jint cpu_index) {
    jclass CpuSenseCachedCls = (*pEnv)->GetObjectClass(pEnv, pObj);
    jfieldID cpu_freq_minID = (*pEnv)->GetFieldID(pEnv, CpuSenseCachedCls, "cpu_freq_min", "[I");
    jobject cpu_freq_minObj = (*pEnv)->GetObjectField(pEnv, pObj, cpu_freq_minID);
    jintArray *cpu_freq_minArr = (jintArray*)(&cpu_freq_minObj);
    jsize len = (*pEnv)->GetArrayLength(pEnv, *cpu_freq_minArr);

    int value = 0;
    if (cpu_index<len) {
      jint *cpu_freq_min = (*pEnv)->GetIntArrayElements(pEnv, *cpu_freq_minArr, 0);
      value = cpu_freq_min[cpu_index];
      if (value!=-1)
        return value;
      else
        throwCpuSenseException(pEnv, (*pEnv)->NewStringUTF(pEnv, "Error while getting cpu_freq_min, value = -1."));
    } else
      throwCpuSenseException(pEnv, (*pEnv)->NewStringUTF(pEnv, "Error while getting cpu_freq_min, cpu_index out of range."));
}

JNIEXPORT jstring JNICALL Java_org_morphone_sense_cpu_CpuSenseCached_getCpuGovernorNative
  (JNIEnv *pEnv, jobject pObj, jint cpu_index) {
    jclass CpuSenseCachedCls = (*pEnv)->GetObjectClass(pEnv, pObj);
    jfieldID cpu_governorID = (*pEnv)->GetFieldID(pEnv, CpuSenseCachedCls, "cpu_governor", "[S");
    jobject cpu_governorObj = (*pEnv)->GetObjectField(pEnv, pObj, cpu_governorID);
    jobjectArray *cpu_governorArr = (jobjectArray*)(&cpu_governorObj);
    jsize len = (*pEnv)->GetArrayLength(pEnv, *cpu_governorArr);

    jstring value;
    if (cpu_index<len) {
      jobject *cpu_governor = (*pEnv)->GetObjectArrayElement(pEnv, *cpu_governorArr, 0);
      value = (jstring)(*pEnv)->GetObjectArrayElement(pEnv, *cpu_governorArr, cpu_index);
      char *str = (*pEnv)->GetStringUTFChars(pEnv, value, 0);
      if (!strcmp(str, "not found"))
        return value;
      else
        throwCpuSenseException(pEnv, (*pEnv)->NewStringUTF(pEnv, "Error while getting cpu_governor, value = -1."));
    } else
      throwCpuSenseException(pEnv, (*pEnv)->NewStringUTF(pEnv, "Error while getting cpu_governor, cpu_index out of range."));
}
JNIEXPORT jfloat JNICALL Java_org_morphone_sense_cpu_CpuSensePolling_getCpuUsageNative
  (JNIEnv *pEnv, jobject pObj, jint cpu_index){
  FILE* meminfo;
    char buffer[100] = {0};
    int value;

    if (!(meminfo = fopen("/proc/stat", "r"))) {
      return -1;
    } 
    int i = 0;
    for (i = 0; i <= cpu_index; i++)
      fgets(buffer, sizeof(buffer), meminfo);
    fgets(buffer, sizeof(buffer), meminfo);
    long int cpu_time[8];
    long previous_idle =0;
    long previous_cpu = 0;
    if (sscanf(buffer, "%*s %l %l %l %l %l %l %l", 
    &cpu_time[0], &cpu_time[1], &cpu_time[2], &cpu_time[3], 
      &cpu_time[4], &cpu_time[5], &cpu_time[6], &cpu_time[7]) == 1) {
    previous_idle = cpu_time[4];
    previous_cpu = cpu_time[2] + cpu_time[3] + cpu_time[1];
  }
  fclose(meminfo);
  
  sleep(360);
  if((*pEnv)->ExceptionOccurred(pEnv)) {
    throwCpuSenseException(pEnv, (*pEnv)->NewStringUTF(pEnv,"Error occurred"));
  } 
  if (!(meminfo = fopen("/proc/stat", "r"))) {
      return -1;
    } 
    for (i = 0; i <= cpu_index; i++)
      fgets(buffer, sizeof(buffer), meminfo);
    fgets(buffer, sizeof(buffer), meminfo);
    long int cpu_time2[8];
    long current_idle = 0;
    long current_cpu = 0;
    if (sscanf(buffer, "%*s %l %l %l %l %l %l %l", 
    &cpu_time2[0], &cpu_time2[1], &cpu_time2[2], &cpu_time2[3], 
      &cpu_time2[4], &cpu_time2[5], &cpu_time2[6], &cpu_time2[7]) == 1) {
    current_idle = cpu_time2[4];
    current_cpu = cpu_time2[2] + cpu_time2[3] + cpu_time2[1];
  }
  fclose(meminfo);

  return (float)(current_cpu - previous_cpu) / ((current_cpu + current_idle) - (previous_cpu + previous_idle))*100;
}

void throwCpuSenseException(JNIEnv *pEnv, jstring eMsg) {
	jclass CpuSenseExceptionCls = (*pEnv)->FindClass(pEnv, "org/morphone/sense/cpu/CpuSenseException");
	if (CpuSenseExceptionCls!=NULL) 
		(*pEnv)->ThrowNew(pEnv, CpuSenseExceptionCls, eMsg);
	(*pEnv)->DeleteLocalRef(pEnv, CpuSenseExceptionCls);
}

int getValue(JNIEnv *pEnv, char *path) {
	FILE* meminfo;
  	char buffer[100] = {0};
  	int value;

  	if (!(meminfo = fopen(path, "r"))) {
  		return -1;
  	}

  	fgets(buffer, sizeof(buffer), meminfo); 	//read 1st line
  	sscanf(buffer, "%d", &value);
  	fclose(meminfo);
	return value;
}

char * getValueString (JNIEnv *pEnv, char *path) {
	FILE* meminfo;
  	// char buffer[100] = {0};
  	char *buffer = (char *) malloc(100);
  	char value[100];

  	meminfo = fopen(path, "r");

  	fgets(buffer, sizeof(buffer), meminfo); 	//read 1st line
  	fclose(meminfo);
  	return buffer;
  	// return (*pEnv)->NewStringUTF(pEnv, buffer);
}
