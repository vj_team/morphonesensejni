#include <jni.h>
#include <stdio.h>
#include <android/log.h>

#include <utilities.h>
#include <LocationSense.h>

JNIEXPORT jboolean JNICALL Java_org_morphone_sense_location_LocationSense_isGPSOnNative
  (JNIEnv *pEnv, jobject pObj) {
  	jclass LocationSenseCls = (*pEnv)->GetObjectClass(pEnv, pObj);
  	jfieldID isGPSOnID = (*pEnv)->GetFieldID(pEnv, LocationSenseCls, "isGPSOn", "Z");
  	return (*pEnv)->GetBooleanField(pEnv, pObj, isGPSOnID);
}

JNIEXPORT jint JNICALL Java_org_morphone_sense_location_LocationSense_getCurrentProviderStatusNative
  (JNIEnv *pEnv, jobject pObj) {
  	jclass LocationSenseCls = (*pEnv)->GetObjectClass(pEnv, pObj);
  	jfieldID gpsStatusID = (*pEnv)->GetFieldID(pEnv, LocationSenseCls, "gpsStatus", "I");
  	int gpsStatus = (*pEnv)->GetIntField(pEnv, pObj, gpsStatusID);
  	if (gpsStatus>=0)
  		return gpsStatus;
    else 
      throwLocationSenseException(pEnv, (*pEnv)->NewStringUTF(pEnv,"Error while getting CurrentProviderStatus"));
}

void throwLocationSenseException(JNIEnv *pEnv, jstring eMsg) {
  jclass LocationSenseExceptionCls = (*pEnv)->FindClass(pEnv, "org/morphone/sense/location/LocationSenseException");
  if (LocationSenseExceptionCls!=NULL) 
    (*pEnv)->ThrowNew(pEnv, LocationSenseExceptionCls, eMsg);
  (*pEnv)->DeleteLocalRef(pEnv, LocationSenseExceptionCls);
}