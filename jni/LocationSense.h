/* DO NOT EDIT THIS FILE - it is machine generated */
#include <jni.h>
/* Header for class org_morphone_sense_location_LocationSense */

#ifndef _Included_org_morphone_sense_location_LocationSense
#define _Included_org_morphone_sense_location_LocationSense
#ifdef __cplusplus
extern "C" {
#endif
/*
 * Class:     org_morphone_sense_location_LocationSense
 * Method:    isGPSOnNative
 * Signature: ()Z
 */
JNIEXPORT jboolean JNICALL Java_org_morphone_sense_location_LocationSense_isGPSOnNative
  (JNIEnv *, jobject);

/*
 * Class:     org_morphone_sense_location_LocationSense
 * Method:    getCurrentProviderStatusNative
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_org_morphone_sense_location_LocationSense_getCurrentProviderStatusNative
  (JNIEnv *, jobject);

void throwLocationSenseException(JNIEnv *, jstring);
#ifdef __cplusplus
}
#endif
#endif
