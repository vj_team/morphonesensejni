#include <jni.h>
#include <stdio.h>
#include <stdlib.h>
#include <android/log.h>

#include <utilities.h>
#include <AccelerometerSense.h>

JNIEXPORT jdouble JNICALL Java_org_morphone_sense_hw_1sensors_AccelerometerSense_getXAccelerationNative
  (JNIEnv *pEnv, jobject pObj) {
  	jclass AccelerometerSenseCls = (*pEnv)->GetObjectClass(pEnv, pObj);
  	jfieldID sensorEventID = (*pEnv)->GetFieldID(pEnv, AccelerometerSenseCls, "sensorEvent", "Landroid/hardware/SensorEvent;");
  	jobject sensorEvent = (*pEnv)->GetObjectField(pEnv, pObj, sensorEventID);
    jclass SensorEventCls = (*pEnv)->GetObjectClass(pEnv, sensorEvent);
  	jfieldID valuesID = (*pEnv)->GetFieldID(pEnv, SensorEventCls, "values", "[F");
  	jobject values = (*pEnv)->GetObjectField(pEnv, sensorEvent, valuesID);
  	jfloatArray* fValueArray = (jfloatArray*)(&values);
  	jsize valuesLen = (*pEnv)->GetArrayLength(pEnv, *fValueArray);
  	float* value = (*pEnv)->GetFloatArrayElements(pEnv, *fValueArray, 0);

  	jfieldID GRAVITY_EARTHID = (*pEnv)->GetStaticFieldID(pEnv, SensorEventCls, "GRAVITY_EARTH", "F");
  	float GRAVITY_EARTH = (*pEnv)->GetStaticFloatField(pEnv, SensorEventCls, GRAVITY_EARTHID);

  	double accX = -(value[0])/GRAVITY_EARTH;
  	double accY = -(value[1])/GRAVITY_EARTH;
  	double accZ = -(value[2])/GRAVITY_EARTH;
  	double totAcc = sqrt((accX*accX)+(accY*accY)+(accZ*accZ));
  	double linX = asin(accX/totAcc);

  	// jfieldID xAsisID = (*pEnv)->GetFieldID(pEnv, AccelerometerSenseCls, "xAsis", "D");
  	// (*pEnv)->SetDoubleField(pEnv, pObj, xAsisID, linX);
  	return linX;
}

JNIEXPORT jdouble JNICALL Java_org_morphone_sense_hw_1sensors_AccelerometerSense_getYAccelerationNative
  (JNIEnv *pEnv, jobject pObj) {
  	jclass AccelerometerSenseCls = (*pEnv)->GetObjectClass(pEnv, pObj);
  	jfieldID sensorEventID = (*pEnv)->GetFieldID(pEnv, AccelerometerSenseCls, "sensorEvent", "Landroid/hardware/SensorEvent;");
  	jobject sensorEvent = (*pEnv)->GetObjectField(pEnv, pObj, sensorEventID);
	jclass SensorEventCls = (*pEnv)->GetObjectClass(pEnv, sensorEvent);
  	jfieldID valuesID = (*pEnv)->GetFieldID(pEnv, SensorEventCls, "values", "[F");
  	jobject values = (*pEnv)->GetObjectField(pEnv, sensorEvent, valuesID);
  	jfloatArray* fValueArray = (jfloatArray*)(&values);
  	jsize valuesLen = (*pEnv)->GetArrayLength(pEnv, *fValueArray);
  	float* value = (*pEnv)->GetFloatArrayElements(pEnv, *fValueArray, 0);

  	jfieldID GRAVITY_EARTHID = (*pEnv)->GetStaticFieldID(pEnv, SensorEventCls, "GRAVITY_EARTH", "F");
  	float GRAVITY_EARTH = (*pEnv)->GetStaticFloatField(pEnv, SensorEventCls, GRAVITY_EARTHID);

  	double accX = -(value[0])/GRAVITY_EARTH;
  	double accY = -(value[1])/GRAVITY_EARTH;
  	double accZ = -(value[2])/GRAVITY_EARTH;
  	double totAcc = sqrt((accX*accX)+(accY*accY)+(accZ*accZ));
  	double linY = asin(accY/totAcc);

  	// jfieldID xAsisID = (*pEnv)->GetFieldID(pEnv, AccelerometerSenseCls, "xAsis", "D");
  	// (*pEnv)->SetDoubleField(pEnv, pObj, xAsisID, linX);
  	return linY;
}

JNIEXPORT jdouble JNICALL Java_org_morphone_sense_hw_1sensors_AccelerometerSense_getZAccelerationNative
  (JNIEnv *pEnv, jobject pObj) {
  	jclass AccelerometerSenseCls = (*pEnv)->GetObjectClass(pEnv, pObj);
  	jfieldID sensorEventID = (*pEnv)->GetFieldID(pEnv, AccelerometerSenseCls, "sensorEvent", "Landroid/hardware/SensorEvent;");
  	jobject sensorEvent = (*pEnv)->GetObjectField(pEnv, pObj, sensorEventID);
	jclass SensorEventCls = (*pEnv)->GetObjectClass(pEnv, sensorEvent);
  	jfieldID valuesID = (*pEnv)->GetFieldID(pEnv, SensorEventCls, "values", "[F");
  	jobject values = (*pEnv)->GetObjectField(pEnv, sensorEvent, valuesID);
  	jfloatArray* fValueArray = (jfloatArray*)(&values);
  	jsize valuesLen = (*pEnv)->GetArrayLength(pEnv, *fValueArray);
  	float* value = (*pEnv)->GetFloatArrayElements(pEnv, *fValueArray, 0);

  	jfieldID GRAVITY_EARTHID = (*pEnv)->GetStaticFieldID(pEnv, SensorEventCls, "GRAVITY_EARTH", "F");
  	float GRAVITY_EARTH = (*pEnv)->GetStaticFloatField(pEnv, SensorEventCls, GRAVITY_EARTHID);

  	double accX = -(value[0])/GRAVITY_EARTH;
  	double accY = -(value[1])/GRAVITY_EARTH;
  	double accZ = -(value[2])/GRAVITY_EARTH;
  	double totAcc = sqrt((accX*accX)+(accY*accY)+(accZ*accZ));
  	double linZ = asin(accZ/totAcc);

  	// jfieldID xAsisID = (*pEnv)->GetFieldID(pEnv, AccelerometerSenseCls, "xAsis", "D");
  	// (*pEnv)->SetDoubleField(pEnv, pObj, xAsisID, linX);
  	return linZ;
}