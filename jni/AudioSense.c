#include <jni.h>
#include <stdio.h>
#include <android/log.h>

#include <utilities.h>
#include <AudioSense.h>

JNIEXPORT jboolean JNICALL Java_org_morphone_sense_audio_AudioSense_isMusicActiveNative
  (JNIEnv *pEnv, jobject pObj) {
	jclass thisCls = (*pEnv)->GetObjectClass(pEnv, pObj);

	jfieldID amID = (*pEnv)->GetFieldID(pEnv, thisCls, "am", "Landroid/media/AudioManager;");
	jobject amObj = (*pEnv)->GetObjectField(pEnv, pObj, amID);
	jclass amCls = (*pEnv)->GetObjectClass(pEnv, amObj);

	jmethodID amIsMusicActiveID =(*pEnv)->GetMethodID(pEnv, amCls, "isMusicActive", "()Z");
	if((*pEnv)->ExceptionOccurred(pEnv)) {
		throwAudioSenseException(pEnv, (*pEnv)->NewStringUTF(pEnv,"Error while getting isMusicActive"));
	}

	jboolean amIsMusicActive = (*pEnv)->CallBooleanMethod(pEnv, amObj, amIsMusicActiveID);
	if((*pEnv)->ExceptionOccurred(pEnv)) {
		throwAudioSenseException(pEnv, (*pEnv)->NewStringUTF(pEnv,"Error while getting isMusicActive"));
	}
	return amIsMusicActive;
}

JNIEXPORT jint JNICALL Java_org_morphone_sense_audio_AudioSense_getMusicVolumeNative
  (JNIEnv *pEnv, jobject pObj) {
	jclass thisCls = (*pEnv)->GetObjectClass(pEnv, pObj);

	jfieldID amID = (*pEnv)->GetFieldID(pEnv, thisCls, "am", "Landroid/media/AudioManager;");
	jobject amObj = (*pEnv)->GetObjectField(pEnv, pObj, amID);
	jclass amCls = (*pEnv)->GetObjectClass(pEnv, amObj);

	jfieldID streamMusicID = (*pEnv)->GetStaticFieldID(pEnv, amCls, "STREAM_MUSIC", "I");
	jint streamMusic = (*pEnv)->GetStaticIntField(pEnv, amCls, streamMusicID);

	jmethodID amGetStreamVolumeID =(*pEnv)->GetMethodID(pEnv, amCls, "getStreamVolume", "(I)I");
	jint amGetStreamVolume = (*pEnv)->CallIntMethod(pEnv, amObj, amGetStreamVolumeID, streamMusic);
	if((*pEnv)->ExceptionOccurred(pEnv)) {
		throwAudioSenseException(pEnv, (*pEnv)->NewStringUTF(pEnv,"Error while getting getMusicVolume"));
	}
	return amGetStreamVolume;
}

JNIEXPORT jint JNICALL Java_org_morphone_sense_audio_AudioSense_getVoiceVolumeNative
  (JNIEnv *pEnv, jobject pObj) {
	jclass thisCls = (*pEnv)->GetObjectClass(pEnv, pObj);
	jfieldID amID = (*pEnv)->GetFieldID(pEnv, thisCls, "am", "Landroid/media/AudioManager;");
	jobject amObj = (*pEnv)->GetObjectField(pEnv, pObj, amID);
	jclass amCls = (*pEnv)->GetObjectClass(pEnv, amObj);

	jfieldID streamVoiceCallID = (*pEnv)->GetStaticFieldID(pEnv, amCls, "STREAM_VOICE_CALL", "I");
	jint streamVoiceCall = (*pEnv)->GetStaticIntField(pEnv, amCls, streamVoiceCallID);

	jmethodID amGetVoiceVolumeID =(*pEnv)->GetMethodID(pEnv, amCls, "getStreamVolume", "(I)I");
	jint amGetVoiceVolume = (*pEnv)->CallIntMethod(pEnv, amObj, amGetVoiceVolumeID, streamVoiceCall);
	if((*pEnv)->ExceptionOccurred(pEnv)) {
		throwAudioSenseException(pEnv, (*pEnv)->NewStringUTF(pEnv,"Error while getting getVoiceVolume"));
	}
	return amGetVoiceVolume;
}

JNIEXPORT jint JNICALL Java_org_morphone_sense_audio_AudioSense_getRingVolumeNative
  (JNIEnv *pEnv, jobject pObj) {
	jclass thisCls = (*pEnv)->GetObjectClass(pEnv, pObj);
	jfieldID amID = (*pEnv)->GetFieldID(pEnv, thisCls, "am", "Landroid/media/AudioManager;");
	jobject amObj = (*pEnv)->GetObjectField(pEnv, pObj, amID);
	jclass amCls = (*pEnv)->GetObjectClass(pEnv, amObj);

	jfieldID streamRingID = (*pEnv)->GetStaticFieldID(pEnv, amCls, "STREAM_RING", "I");
	jint streamRing = (*pEnv)->GetStaticIntField(pEnv, amCls, streamRingID);
	
	jmethodID amGetRingVolumeID =(*pEnv)->GetMethodID(pEnv, amCls, "getStreamVolume", "(I)I");
	jint amGetRingVolume = (*pEnv)->CallIntMethod(pEnv, amObj, amGetRingVolumeID, streamRing);
	if((*pEnv)->ExceptionOccurred(pEnv)) {
		throwAudioSenseException(pEnv, (*pEnv)->NewStringUTF(pEnv,"Error while getting getRingVolume"));
	}
	return amGetRingVolume;
}

JNIEXPORT jboolean JNICALL Java_org_morphone_sense_audio_AudioSense_isSpeakerOnNative
  (JNIEnv *pEnv, jobject pObj){

	jclass thisCls = (*pEnv)->GetObjectClass(pEnv, pObj);
	jfieldID amID = (*pEnv)->GetFieldID(pEnv, thisCls, "am", "Landroid/media/AudioManager;");
	jobject amObj = (*pEnv)->GetObjectField(pEnv, pObj, amID);
	jclass amCls = (*pEnv)->GetObjectClass(pEnv, amObj);

	jmethodID amIsSpeakerOnID =(*pEnv)->GetMethodID(pEnv, amCls, "isSpeakerphoneOn", "()Z");
	jboolean amIsSpeakerOn = (*pEnv)->CallBooleanMethod(pEnv, amObj, amIsSpeakerOnID);
	if((*pEnv)->ExceptionOccurred(pEnv)) {
		throwAudioSenseException(pEnv, (*pEnv)->NewStringUTF(pEnv,"Error while getting isSpeakerOn"));
	}
	return amIsSpeakerOn;
}

JNIEXPORT jint JNICALL Java_org_morphone_sense_audio_AudioSense_getModeNative
  (JNIEnv *pEnv, jobject pObj) {

	jclass thisCls = (*pEnv)->GetObjectClass(pEnv, pObj);

	jfieldID amID = (*pEnv)->GetFieldID(pEnv, thisCls, "am", "Landroid/media/AudioManager;");
	jobject amObj = (*pEnv)->GetObjectField(pEnv, pObj, amID);
	jclass amCls = (*pEnv)->GetObjectClass(pEnv, amObj);

	jmethodID amGetModeID =(*pEnv)->GetMethodID(pEnv, amCls, "getMode", "()I");
	jint amGetMode = (*pEnv)->CallIntMethod(pEnv, amObj, amGetModeID);
	if((*pEnv)->ExceptionOccurred(pEnv)) {
		throwAudioSenseException(pEnv, (*pEnv)->NewStringUTF(pEnv,"Error while getting getMode"));
	}
	return amGetMode;
}

void throwAudioSenseException(JNIEnv *pEnv, jstring eMsg) {
	jclass AudioSenseExceptionCls = (*pEnv)->FindClass(pEnv, "org/morphone/sense/audio/AudioSenseException");
	if (AudioSenseExceptionCls!=NULL) 
		(*pEnv)->ThrowNew(pEnv, AudioSenseExceptionCls, eMsg);
	(*pEnv)->DeleteLocalRef(pEnv, AudioSenseExceptionCls);
}
