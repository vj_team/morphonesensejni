#include <jni.h>
#include <stdio.h>
#include <android/log.h>

#include <utilities.h>
#include <MemorySense.h>

JNIEXPORT jlong JNICALL Java_org_morphone_sense_memory_MemorySense_getUsedMemoryNative
  (JNIEnv *pEnv, jobject pObj) {
  	// jclass MemorySenseCls = (*pEnv)->GetObjectClass(pEnv, pObj);

  	// jfieldID MEM_USAGE_INFOID = (*pEnv)->GetStaticFieldID(pEnv, MemorySenseCls, "MEM_USAGE_INFO", "Ljava/lang/String;");
  	// jstring MEM_USAGE_INFO = (*pEnv)->GetStaticObjectField(pEnv, MemorySenseCls, MEM_USAGE_INFOID);

	// read /proc/meminfo
  	FILE* meminfo;
  	char buffer[100] = {0};
  	char* end;
  	int memTotal;
  	int memFree;

  	// try to read /proc/meminfo
  	// if (!(meminfo = fopen(MEM_USAGE_INFO, "r"))) {
  	if (!(meminfo = fopen("/proc/meminfo", "r"))) {
  		__android_log_print(ANDROID_LOG_INFO, "Morphone", "exit. error occurred");
  		return -1;
  	}

  	fgets(buffer, sizeof(buffer), meminfo); 	//read 1st line
  	if (sscanf(buffer, "MemTotal: %d kB", &memTotal) == 1) {
  		__android_log_print(ANDROID_LOG_INFO, "Morphone", "native: MemTotal = %d", memTotal);
  	}
  	fgets(buffer, sizeof(buffer), meminfo); 	//read 1st line
  	if (sscanf(buffer, "MemFree: %d kB", &memFree) == 1) {
  		__android_log_print(ANDROID_LOG_INFO, "Morphone", "native: MemFree = %d", memFree);
  	} 
	  
  	fclose(meminfo);
  	return (memTotal-memFree);
}

JNIEXPORT jlong JNICALL Java_org_morphone_sense_memory_MemorySense_getTotalMemoryNative
  (JNIEnv *pEnv, jobject pObj) {
//  	jclass MemorySenseCls = (*pEnv)->GetObjectClass(pEnv, pObj);
//  	jfieldID totalMemoryID = (*pEnv)->GetFieldID(pEnv, MemorySenseCls, "totalMemory", "L");
//  	long totalMemory = (*pEnv)->GetLongField(pEnv, pObj, totalMemoryID);
//  	if(totalMemory>=0)
//  		return totalMemory;
//    else
//      throwMemorySenseException(pEnv, (*pEnv)->NewStringUTF(pEnv,"Error while getting mem total"));
	FILE* meminfo;
	char buffer[100] = {0};
	char* end;
	int memTotal;
	int memFree;

	// try to read /proc/meminfo
	// if (!(meminfo = fopen(MEM_USAGE_INFO, "r"))) {
	if (!(meminfo = fopen("/proc/meminfo", "r"))) {
		__android_log_print(ANDROID_LOG_INFO, "Morphone", "exit. error occurred");
		return -1;
	}

	fgets(buffer, sizeof(buffer), meminfo); 	//read 1st line
	if (sscanf(buffer, "MemTotal: %d kB", &memTotal) == 1) {
		__android_log_print(ANDROID_LOG_INFO, "Morphone", "native: MemTotal = %d", memTotal);
	}
	fclose(meminfo);
	return memTotal;
  
}

void throwMemorySenseException(JNIEnv *pEnv, jstring eMsg) {
  jclass MemorySenseExceptionCls = (*pEnv)->FindClass(pEnv, "org/morphone/sense/memory/MemorySenseException");
  if (MemorySenseExceptionCls!=NULL) 
    (*pEnv)->ThrowNew(pEnv, MemorySenseExceptionCls, eMsg);
  (*pEnv)->DeleteLocalRef(pEnv, MemorySenseExceptionCls);
}
