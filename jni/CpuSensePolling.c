#include <jni.h>
#include <stdio.h>
#include <android/log.h>

#include <utilities.h>
#include <CpuSensePolling.h>

JNIEXPORT jint JNICALL Java_org_morphone_sense_cpu_CpuSensePolling_getNumberOfCPUsNative
  (JNIEnv *pEnv, jobject pObj) {
  	jclass CpuSensePollingCls = (*pEnv)->GetObjectClass(pEnv, pObj);
  	jfieldID cpu_numberID = (*pEnv)->GetFieldID(pEnv, CpuSensePollingCls, "cpu_number", "I");
  	int cpu_number = (*pEnv)->GetIntField(pEnv, pObj, cpu_numberID);
  	if (cpu_number > 0)
  		return cpu_number;
  	else
  		throwCpuSenseException(pEnv, (*pEnv)->NewStringUTF(pEnv, "Error while getting cpu number"));
}

JNIEXPORT jint JNICALL Java_org_morphone_sense_cpu_CpuSensePolling_getCpuCurrentFrequencyNative
  (JNIEnv *pEnv, jobject pObj, jint cpu_index) {
  	char path[256];
  	sprintf(path, "/sys/devices/system/cpu/cpu%d/cpufreq/scaling_cur_freq", cpu_index);
  	// strcpy(path, "/sys/devices/system/cpu/cpu");
  	// strcat(path, cpu_index);
	// strcat(path, "/cpufreq/scaling_cur_freq");
	int ret = getValue(pEnv, path);
	if((*pEnv)->ExceptionOccurred(pEnv)) {
		return -1;
	} else
	return ret;
  }

JNIEXPORT jint JNICALL Java_org_morphone_sense_cpu_CpuSensePolling_getCpuMaxScalingNative
  (JNIEnv *pEnv, jobject pObj, jint cpu_index) {
	char path[256];
  	sprintf(path, "/sys/devices/system/cpu/cpu%d/cpufreq/scaling_max_freq", cpu_index);
  	// strcpy(path, "/sys/devices/system/cpu/cpu");
  	// strcat(path, cpu_index);
	// strcat(path, "/cpufreq/scaling_cur_freq");
	int ret = getValue(pEnv, path);
	if((*pEnv)->ExceptionOccurred(pEnv)) {
		return -1;
	} else
	return ret;	
}

JNIEXPORT jint JNICALL Java_org_morphone_sense_cpu_CpuSensePolling_getCpuMinScalingNative
  (JNIEnv *pEnv, jobject pObj, jint cpu_index) {
	char path[256];
  	sprintf(path, "/sys/devices/system/cpu/cpu%d/cpufreq/scaling_min_freq", cpu_index);
  	// strcpy(path, "/sys/devices/system/cpu/cpu");
  	// strcat(path, cpu_index);
	// strcat(path, "/cpufreq/scaling_cur_freq");
	int ret = getValue(pEnv, path);
	if((*pEnv)->ExceptionOccurred(pEnv)) {
		return -1;
	} else
	return ret;	
}

JNIEXPORT jint JNICALL Java_org_morphone_sense_cpu_CpuSensePolling_getCpuMaxFrequencyNative
  (JNIEnv *pEnv, jobject pObj, jint cpu_index) {
	char path[256];
  	sprintf(path, "/sys/devices/system/cpu/cpu%d/cpufreq/cpuinfo_max_freq", cpu_index);
  	// strcpy(path, "/sys/devices/system/cpu/cpu");
  	// strcat(path, cpu_index);
	// strcat(path, "/cpufreq/scaling_cur_freq");
	int ret = getValue(pEnv, path);
	if((*pEnv)->ExceptionOccurred(pEnv)) {
		return -1;
	} else
	return ret;	
}

JNIEXPORT jint JNICALL Java_org_morphone_sense_cpu_CpuSensePolling_getCpuMinFrequencyNative
  (JNIEnv *pEnv, jobject pObj, jint cpu_index) {
	char path[256];
  	sprintf(path, "/sys/devices/system/cpu/cpu%d/cpufreq/cpuinfo_min_freq", cpu_index);
  	// strcpy(path, "/sys/devices/system/cpu/cpu");
  	// strcat(path, cpu_index);
	// strcat(path, "/cpufreq/scaling_cur_freq");
	int ret = getValue(pEnv, path);
	if((*pEnv)->ExceptionOccurred(pEnv)) {
		return -1;
	} else
	return ret;	
}

JNIEXPORT jobject JNICALL Java_org_morphone_sense_cpu_CpuSensePolling_getCpuGovernorNative
  (JNIEnv *pEnv, jobject pObj, jint cpu_index) {
	char path[256];
  	sprintf(path, "/sys/devices/system/cpu/cpu%d/cpufreq/cpuinfo_min_freq", cpu_index);
  	// strcpy(path, "/sys/devices/system/cpu/cpu");
  	// strcat(path, cpu_index);
	// strcat(path, "/cpufreq/scaling_cur_freq");
	jstring ret = (*pEnv)->NewStringUTF(pEnv, getValueString(pEnv, path));
	if((*pEnv)->ExceptionOccurred(pEnv)) {
		return -1;
	} else
	return ret;		
}

JNIEXPORT jfloat JNICALL Java_org_morphone_sense_cpu_CpuSensePolling_getCpuUsageNative
  (JNIEnv *pEnv, jobject pObj, jint cpu_index){
	FILE* meminfo;
  	char buffer[100] = {0};
  	int value;

  	if (!(meminfo = fopen("/proc/stat", "r"))) {
  		return -1;
  	}	
  	int i = 0;
  	for (i = 0; i <= cpu_index; i++)
  		fgets(buffer, sizeof(buffer), meminfo);
  	fgets(buffer, sizeof(buffer), meminfo);
  	long int cpu_time[8];
  	long previous_idle =0;
  	long previous_cpu = 0;
  	if (sscanf(buffer, "%*s %l %l %l %l %l %l %l", 
	  &cpu_time[0], &cpu_time[1], &cpu_time[2], &cpu_time[3], 
  	  &cpu_time[4], &cpu_time[5], &cpu_time[6], &cpu_time[7]) == 1) {
		previous_idle = cpu_time[4];
		previous_cpu = cpu_time[2] + cpu_time[3] + cpu_time[1];
	}
	fclose(meminfo);
	
	sleep(360);
	if((*pEnv)->ExceptionOccurred(pEnv)) {
		throwCpuSenseException(pEnv, (*pEnv)->NewStringUTF(pEnv,"Error occurred"));
	} 
	if (!(meminfo = fopen("/proc/stat", "r"))) {
  		return -1;
  	}	
  	for (i = 0; i <= cpu_index; i++)
  		fgets(buffer, sizeof(buffer), meminfo);
  	fgets(buffer, sizeof(buffer), meminfo);
  	long int cpu_time2[8];
  	long current_idle = 0;
  	long current_cpu = 0;
  	if (sscanf(buffer, "%*s %l %l %l %l %l %l %l", 
	  &cpu_time2[0], &cpu_time2[1], &cpu_time2[2], &cpu_time2[3], 
  	  &cpu_time2[4], &cpu_time2[5], &cpu_time2[6], &cpu_time2[7]) == 1) {
		current_idle = cpu_time2[4];
		current_cpu = cpu_time2[2] + cpu_time2[3] + cpu_time2[1];
	}
	fclose(meminfo);

	return (float)(current_cpu - previous_cpu) / ((current_cpu + current_idle) - (previous_cpu + previous_idle))*100;
}


void throwCpuSenseException(JNIEnv *pEnv, jstring eMsg) {
	jclass CpuSenseExceptionCls = (*pEnv)->FindClass(pEnv, "org/morphone/sense/cpu/CpuSenseException");
	if (CpuSenseExceptionCls!=NULL) 
		(*pEnv)->ThrowNew(pEnv, CpuSenseExceptionCls, eMsg);
	(*pEnv)->DeleteLocalRef(pEnv, CpuSenseExceptionCls);
}

int getValue(JNIEnv *pEnv, char *path) {
	FILE* meminfo;
  	char buffer[100] = {0};
  	int value;

  	if (!(meminfo = fopen(path, "r"))) {
  		return -1;
  	}

  	fgets(buffer, sizeof(buffer), meminfo); 	//read 1st line
  	sscanf(buffer, "%d", &value);
  	fclose(meminfo);
	return value;
}

char * getValueString (JNIEnv *pEnv, char *path) {
	FILE* meminfo;
  	// char buffer[100] = {0};
  	char *buffer = (char *) malloc(100);
  	char value[100];

  	meminfo = fopen(path, "r");

  	fgets(buffer, sizeof(buffer), meminfo); 	//read 1st line
  	fclose(meminfo);
  	return buffer;
  	// return (*pEnv)->NewStringUTF(pEnv, buffer);
}