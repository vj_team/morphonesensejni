#include <jni.h>
#include <stdio.h>
#include <android/log.h>

#include <utilities.h>
#include <WifiSense.h>

JNIEXPORT jint JNICALL Java_org_morphone_sense_wifi_WifiSenseReceiver_getNetworkIdNative
  (JNIEnv *pEnv, jobject pObj) {
	jclass WifiSenseReceiverCls = (*pEnv)->GetObjectClass(pEnv, pObj);
	jfieldID isEnabledID = (*pEnv)->GetFieldID(pEnv, WifiSenseReceiverCls, "isEnabled", "I");
	int isEnabled = (*pEnv)->GetIntField(pEnv, pObj, isEnabledID);
	jfieldID isConnectedID = (*pEnv)->GetFieldID(pEnv, WifiSenseReceiverCls, "isConnected", "I");
	int isConnected = (*pEnv)->GetIntField(pEnv, pObj, isConnectedID);

	if (isEnabled==1 && isConnected==1) {
		int networkId = -1;

		jfieldID wifiInfoID = (*pEnv)->GetFieldID(pEnv, WifiSenseReceiverCls, "wifiInfo", "Landroid/net/wifi/WifiInfo;");
		jobject wifiInfo = (*pEnv)->GetObjectField(pEnv, pObj, wifiInfoID);
		jclass WifiInfoCls = (*pEnv)->GetObjectClass(pEnv, wifiInfo);
		jmethodID getNetworkIdMID = (*pEnv)->GetMethodID(pEnv, WifiInfoCls, "getNetworkId", "()I");
		networkId = (*pEnv)->CallIntMethod(pEnv, wifiInfo, getNetworkIdMID);
		if (networkId>=0)
			return networkId;
	} else
		return -1;
}

JNIEXPORT jint JNICALL Java_org_morphone_sense_wifi_WifiSenseReceiver_getSignalStrenghtNative
  (JNIEnv *pEnv, jobject pObj) {
	jclass WifiSenseReceiverCls = (*pEnv)->GetObjectClass(pEnv, pObj);
	jfieldID isEnabledID = (*pEnv)->GetFieldID(pEnv, WifiSenseReceiverCls, "isEnabled", "I");
	int isEnabled = (*pEnv)->GetIntField(pEnv, pObj, isEnabledID);
	jfieldID isConnectedID = (*pEnv)->GetFieldID(pEnv, WifiSenseReceiverCls, "isConnected", "I");
	int isConnected = (*pEnv)->GetIntField(pEnv, pObj, isConnectedID);

	if (isEnabled==1 && isConnected==1) {
		jfieldID wifiInfoID = (*pEnv)->GetFieldID(pEnv, WifiSenseReceiverCls, "wifiInfo", "Landroid/net/wifi/WifiInfo;");
		jobject wifiInfo = (*pEnv)->GetObjectField(pEnv, pObj, wifiInfoID);
		jclass WifiInfoCls = (*pEnv)->GetObjectClass(pEnv, wifiInfo);
		jmethodID getRssiMID = (*pEnv)->GetMethodID(pEnv, WifiInfoCls, "getRssi", "()I");
		return (*pEnv)->CallIntMethod(pEnv, wifiInfo, getRssiMID);
	} else
		return -1;
}

JNIEXPORT jint JNICALL Java_org_morphone_sense_wifi_WifiSenseReceiver_getLinkSpeedNative
  (JNIEnv *pEnv, jobject pObj) {
	jclass WifiSenseReceiverCls = (*pEnv)->GetObjectClass(pEnv, pObj);
	jfieldID isEnabledID = (*pEnv)->GetFieldID(pEnv, WifiSenseReceiverCls, "isEnabled", "I");
	int isEnabled = (*pEnv)->GetIntField(pEnv, pObj, isEnabledID);
	jfieldID isConnectedID = (*pEnv)->GetFieldID(pEnv, WifiSenseReceiverCls, "isConnected", "I");
	int isConnected = (*pEnv)->GetIntField(pEnv, pObj, isConnectedID);

	if (isEnabled==1 && isConnected==1) {
		jfieldID wifiInfoID = (*pEnv)->GetFieldID(pEnv, WifiSenseReceiverCls, "wifiInfo", "Landroid/net/wifi/WifiInfo;");
		jobject wifiInfo = (*pEnv)->GetObjectField(pEnv, pObj, wifiInfoID);
		jclass WifiInfoCls = (*pEnv)->GetObjectClass(pEnv, wifiInfo);
		jmethodID getLinkSpeedMID = (*pEnv)->GetMethodID(pEnv, WifiInfoCls, "getLinkSpeed", "()I");
		return (*pEnv)->CallIntMethod(pEnv, wifiInfo, getLinkSpeedMID);
	} else
		return -1;
}

JNIEXPORT jint JNICALL Java_org_morphone_sense_wifi_WifiSenseReceiver_getIPAddressNative
  (JNIEnv *pEnv, jobject pObj) {
	jclass WifiSenseReceiverCls = (*pEnv)->GetObjectClass(pEnv, pObj);
	jfieldID isEnabledID = (*pEnv)->GetFieldID(pEnv, WifiSenseReceiverCls, "isEnabled", "I");
	int isEnabled = (*pEnv)->GetIntField(pEnv, pObj, isEnabledID);
	jfieldID isConnectedID = (*pEnv)->GetFieldID(pEnv, WifiSenseReceiverCls, "isConnected", "I");
	int isConnected = (*pEnv)->GetIntField(pEnv, pObj, isConnectedID);

	if (isEnabled==1 && isConnected==1) {
		jfieldID wifiInfoID = (*pEnv)->GetFieldID(pEnv, WifiSenseReceiverCls, "wifiInfo", "Landroid/net/wifi/WifiInfo;");
		jobject wifiInfo = (*pEnv)->GetObjectField(pEnv, pObj, wifiInfoID);
		jclass WifiInfoCls = (*pEnv)->GetObjectClass(pEnv, wifiInfo);
		jmethodID getIPAddressMID = (*pEnv)->GetMethodID(pEnv, WifiInfoCls, "getIpAddress", "()I");
		return (*pEnv)->CallIntMethod(pEnv, wifiInfo, getIPAddressMID);
	} else
		return -1;
}

JNIEXPORT jlong JNICALL Java_org_morphone_sense_wifi_WifiSenseReceiver_getTxBytesNative
  (JNIEnv *pEnv, jobject pObj) {
	jclass WifiSenseReceiverCls = (*pEnv)->GetObjectClass(pEnv, pObj);
	jfieldID isEnabledID = (*pEnv)->GetFieldID(pEnv, WifiSenseReceiverCls, "isEnabled", "I");
	int isEnabled = (*pEnv)->GetIntField(pEnv, pObj, isEnabledID);
	jfieldID isConnectedID = (*pEnv)->GetFieldID(pEnv, WifiSenseReceiverCls, "isConnected", "I");
	int isConnected = (*pEnv)->GetIntField(pEnv, pObj, isConnectedID);
	jfieldID txBytesID = (*pEnv)->GetFieldID(pEnv, WifiSenseReceiverCls, "txBytes", "J");
	long txBytes = (*pEnv)->GetLongField(pEnv, pObj, txBytesID);

	if (isEnabled==1 && isConnected==1) {
		jclass TrafficStatsCls = (*pEnv)->FindClass(pEnv, "android/net/TrafficStats");
		jfieldID UNSUPPORTEDID = (*pEnv)->GetStaticFieldID(pEnv, TrafficStatsCls, "UNSUPPORTED", "I");
		int UNSUPPORTED = (*pEnv)->GetStaticIntField(pEnv, TrafficStatsCls, UNSUPPORTEDID);

		long totalTxBytes = UNSUPPORTED;
		long mobileTxBytes = UNSUPPORTED;

		jmethodID getTotalTxBytesMID = (*pEnv)->GetStaticMethodID(pEnv, TrafficStatsCls, "getTotalTxBytes", "()J");
		jmethodID getMobileTxBytesMID = (*pEnv)->GetStaticMethodID(pEnv, TrafficStatsCls, "getMobileTxBytes", "()J");
		totalTxBytes = (*pEnv)->CallStaticLongMethod(pEnv, TrafficStatsCls, getTotalTxBytesMID);
		mobileTxBytes = (*pEnv)->CallStaticLongMethod(pEnv, TrafficStatsCls, getMobileTxBytesMID);

		if (totalTxBytes==UNSUPPORTED || mobileTxBytes==UNSUPPORTED)
			(*pEnv)->SetLongField(pEnv, pObj, txBytesID, -1);
		else {
			long temp = (totalTxBytes-mobileTxBytes);
			(*pEnv)->SetLongField(pEnv, pObj, txBytesID, temp);
		}

		if (txBytes >= 0)
			return txBytes;
	} else
		return txBytes;
}

JNIEXPORT jlong JNICALL Java_org_morphone_sense_wifi_WifiSenseReceiver_calculateTxBytesNative
  (JNIEnv *pEnv, jobject pObj) {
	jclass TrafficStatsCls = (*pEnv)->FindClass(pEnv, "android/net/TrafficStats");
	jfieldID UNSUPPORTEDID = (*pEnv)->GetStaticFieldID(pEnv, TrafficStatsCls, "UNSUPPORTED", "I");
	int UNSUPPORTED = (*pEnv)->GetStaticIntField(pEnv, TrafficStatsCls, UNSUPPORTEDID);

	long totalTxBytes = UNSUPPORTED;
	long mobileTxBytes = UNSUPPORTED;

	jmethodID getTotalTxBytesMID = (*pEnv)->GetStaticMethodID(pEnv, TrafficStatsCls, "getTotalTxBytes", "()J");
	jmethodID getMobileTxBytesMID = (*pEnv)->GetStaticMethodID(pEnv, TrafficStatsCls, "getMobileTxBytes", "()J");
	totalTxBytes = (*pEnv)->CallStaticLongMethod(pEnv, TrafficStatsCls, getTotalTxBytesMID);
	mobileTxBytes = (*pEnv)->CallStaticLongMethod(pEnv, TrafficStatsCls, getMobileTxBytesMID);

	if (totalTxBytes==UNSUPPORTED || mobileTxBytes==UNSUPPORTED)
		return -1;
	else
		return (totalTxBytes-mobileTxBytes);
}

JNIEXPORT jlong JNICALL Java_org_morphone_sense_wifi_WifiSenseReceiver_getRxBytesNative
  (JNIEnv *pEnv, jobject pObj) {
	jclass WifiSenseReceiverCls = (*pEnv)->GetObjectClass(pEnv, pObj);
	jfieldID isEnabledID = (*pEnv)->GetFieldID(pEnv, WifiSenseReceiverCls, "isEnabled", "I");
	int isEnabled = (*pEnv)->GetIntField(pEnv, pObj, isEnabledID);
	jfieldID isConnectedID = (*pEnv)->GetFieldID(pEnv, WifiSenseReceiverCls, "isConnected", "I");
	int isConnected = (*pEnv)->GetIntField(pEnv, pObj, isConnectedID);
	jfieldID rxBytesID = (*pEnv)->GetFieldID(pEnv, WifiSenseReceiverCls, "rxBytes", "J");
	long rxBytes = (*pEnv)->GetLongField(pEnv, pObj, rxBytesID);
	jclass TrafficStatsCls = (*pEnv)->FindClass(pEnv, "android/net/TrafficStats");

	if (isEnabled==1 && isConnected==1) {
		jfieldID UNSUPPORTEDID = (*pEnv)->GetStaticFieldID(pEnv, TrafficStatsCls, "UNSUPPORTED", "I");
		int UNSUPPORTED = (*pEnv)->GetStaticIntField(pEnv, TrafficStatsCls, UNSUPPORTEDID);

		long totalRxBytes = UNSUPPORTED;
		long mobileRxBytes = UNSUPPORTED;

		jmethodID getTotalRxBytesMID = (*pEnv)->GetStaticMethodID(pEnv, TrafficStatsCls, "getTotalRxBytes", "()J");
		jmethodID getMobileRxBytesMID = (*pEnv)->GetStaticMethodID(pEnv, TrafficStatsCls, "getMobileRxBytes", "()J");
		totalRxBytes = (*pEnv)->CallStaticLongMethod(pEnv, TrafficStatsCls, getTotalRxBytesMID);
		mobileRxBytes = (*pEnv)->CallStaticLongMethod(pEnv, TrafficStatsCls, getMobileRxBytesMID);

		if (totalRxBytes==UNSUPPORTED || mobileRxBytes==UNSUPPORTED)
			(*pEnv)->SetLongField(pEnv, pObj, rxBytesID, -1);
		else {
			long temp = (totalRxBytes-mobileRxBytes);
			(*pEnv)->SetLongField(pEnv, pObj, rxBytesID, temp);
		}

		if (rxBytes >= 0)
			return rxBytes;
	} else
		return rxBytes;
}

JNIEXPORT jlong JNICALL Java_org_morphone_sense_wifi_WifiSenseReceiver_calculateRxBytesNative
  (JNIEnv *pEnv, jobject pObj) {
	jclass TrafficStatsCls = (*pEnv)->FindClass(pEnv, "android/net/TrafficStats");
	jfieldID UNSUPPORTEDID = (*pEnv)->GetStaticFieldID(pEnv, TrafficStatsCls, "UNSUPPORTED", "I");
	int UNSUPPORTED = (*pEnv)->GetStaticIntField(pEnv, TrafficStatsCls, UNSUPPORTEDID);

	long totalRxBytes = UNSUPPORTED;
	long mobileRxBytes = UNSUPPORTED;

	jmethodID getTotalRxBytesMID = (*pEnv)->GetStaticMethodID(pEnv, TrafficStatsCls, "getTotalRxBytes", "()J");
	jmethodID getMobileRxBytesMID = (*pEnv)->GetStaticMethodID(pEnv, TrafficStatsCls, "getMobileRxBytes", "()J");
	totalRxBytes = (*pEnv)->CallStaticLongMethod(pEnv, TrafficStatsCls, getTotalRxBytesMID);
	mobileRxBytes = (*pEnv)->CallStaticLongMethod(pEnv, TrafficStatsCls, getMobileRxBytesMID);

	if (totalRxBytes==UNSUPPORTED || mobileRxBytes==UNSUPPORTED)
		return -1;
	else
		return (totalRxBytes-mobileRxBytes);
}

JNIEXPORT jboolean JNICALL Java_org_morphone_sense_wifi_WifiSenseReceiver_isConnectedNative
  (JNIEnv *pEnv, jobject pObj) {
	jclass WifiSenseReceiverCls = (*pEnv)->GetObjectClass(pEnv, pObj);
	jfieldID isEnabledID = (*pEnv)->GetFieldID(pEnv, WifiSenseReceiverCls, "isEnabled", "I");
	int isEnabled = (*pEnv)->GetIntField(pEnv, pObj, isEnabledID);
	jfieldID isConnectedID = (*pEnv)->GetFieldID(pEnv, WifiSenseReceiverCls, "isConnected", "I");
	int isConnected = (*pEnv)->GetIntField(pEnv, pObj, isConnectedID);

	if (isEnabled==1 && isConnected==1)
		return 1;
	else if (isConnected>=0)
		return 0;
	else
		return;
}

JNIEXPORT jboolean JNICALL Java_org_morphone_sense_wifi_WifiSenseReceiver_isActiveNative
  (JNIEnv *pEnv, jobject pObj) {
	jclass WifiSenseReceiverCls = (*pEnv)->GetObjectClass(pEnv, pObj);
	jfieldID isEnabledID = (*pEnv)->GetFieldID(pEnv, WifiSenseReceiverCls, "isEnabled", "I");
	int isEnabled = (*pEnv)->GetIntField(pEnv, pObj, isEnabledID);

	if (isEnabled==1)
		return 1;
	else if (isEnabled==0)
		return 0;
}

JNIEXPORT jstring JNICALL Java_org_morphone_sense_wifi_WifiSenseReceiver_getMACAddressNative
  (JNIEnv *pEnv, jobject pObj) {
	jclass WifiSenseCls = (*pEnv)->GetObjectClass(pEnv, pObj);
	jfieldID macAddressID = (*pEnv)->GetFieldID(pEnv, WifiSenseCls, "macAddress", "Ljava/lang/String;");
	jstring macAddress = (jstring)(*pEnv)->GetObjectField(pEnv, pObj, macAddressID);
	if (macAddress!=NULL)
		return macAddress;
}

JNIEXPORT void JNICALL Java_org_morphone_sense_wifi_WifiSenseReceiver_onReceiveNative
  (JNIEnv *pEnv, jobject pObj, jobject pContext, jobject pIntent) {
	jclass WifiSenseReceiverCls = (*pEnv)->GetObjectClass(pEnv, pObj);
	jfieldID wifiInfoID = (*pEnv)->GetFieldID(pEnv, WifiSenseReceiverCls, "wifiInfo", "Landroid/net/wifi/WifiInfo;");
	jfieldID isEnabledID = (*pEnv)->GetFieldID(pEnv, WifiSenseReceiverCls, "isEnabled", "I");
	jfieldID isConnectedID = (*pEnv)->GetFieldID(pEnv, WifiSenseReceiverCls, "isConnected", "I");
	jfieldID wifiManagerID = (*pEnv)->GetFieldID(pEnv, WifiSenseReceiverCls, "wifiManager", "Landroid/net/wifi/WifiManager;");
	jobject wifiManager = (*pEnv)->GetObjectField(pEnv, pObj, wifiManagerID);
	jclass WifiManagerCls = (*pEnv)->GetObjectClass(pEnv, wifiManager);
	jmethodID getConnectionInfoMID = (*pEnv)->GetMethodID(pEnv, WifiManagerCls, "getConnectionInfo", "()Landroid/net/wifi/WifiInfo;");
	jclass intentCls = (*pEnv)->GetObjectClass(pEnv, pIntent);
	jmethodID getActionMID = (*pEnv)->GetMethodID(pEnv, intentCls, "getAction", "()Ljava/lang/String;");
	jstring action = (jstring)(*pEnv)->CallObjectMethod(pEnv, pIntent, getActionMID);

//	jclass WifiManagerCls = (*pEnv)->FindClass(pEnv, "android/net/wifi/WifiManager");
	jfieldID SCAN_RESULTS_AVAILABLE_ACTIONID = (*pEnv)->GetStaticFieldID(pEnv, WifiManagerCls, "SCAN_RESULTS_AVAILABLE_ACTION", "Ljava/lang/String;");
	jstring SCAN_RESULTS_AVAILABLE_ACTION = (jstring)(*pEnv)->GetStaticObjectField(pEnv, WifiManagerCls, SCAN_RESULTS_AVAILABLE_ACTIONID);
	jfieldID WIFI_STATE_CHANGED_ACTIONID = (*pEnv)->GetStaticFieldID(pEnv, WifiManagerCls, "WIFI_STATE_CHANGED_ACTION", "Ljava/lang/String;");
	jstring WIFI_STATE_CHANGED_ACTION = (jstring)(*pEnv)->GetStaticObjectField(pEnv, WifiManagerCls, WIFI_STATE_CHANGED_ACTIONID);
	jfieldID NETWORK_STATE_CHANGED_ACTIONID = (*pEnv)->GetStaticFieldID(pEnv, WifiManagerCls, "NETWORK_STATE_CHANGED_ACTION", "Ljava/lang/String;");
	jstring NETWORK_STATE_CHANGED_ACTION = (jstring)(*pEnv)->GetStaticObjectField(pEnv, WifiManagerCls, NETWORK_STATE_CHANGED_ACTIONID);
	jfieldID EXTRA_NETWORK_INFOID = (*pEnv)->GetStaticFieldID(pEnv, WifiManagerCls, "EXTRA_NETWORK_INFO", "Ljava/lang/String;");
	jstring EXTRA_NETWORK_INFO = (jstring)(*pEnv)->GetStaticObjectField(pEnv, WifiManagerCls, EXTRA_NETWORK_INFOID);
	jfieldID EXTRA_WIFI_STATEID = (*pEnv)->GetStaticFieldID(pEnv, WifiManagerCls, "EXTRA_WIFI_STATE", "Ljava/lang/String;");
	jstring EXTRA_WIFI_STATE = (jstring)(*pEnv)->GetStaticObjectField(pEnv, WifiManagerCls, EXTRA_WIFI_STATEID);

	jfieldID WIFI_STATE_DISABLEDID = (*pEnv)->GetStaticFieldID(pEnv, WifiManagerCls, "WIFI_STATE_DISABLED", "I");
	int WIFI_STATE_DISABLED = (*pEnv)->GetStaticIntField(pEnv, WifiManagerCls, WIFI_STATE_DISABLEDID);
	jfieldID WIFI_STATE_DISABLINGID = (*pEnv)->GetStaticFieldID(pEnv, WifiManagerCls, "WIFI_STATE_DISABLING", "I");
	int WIFI_STATE_DISABLING = (*pEnv)->GetStaticIntField(pEnv, WifiManagerCls, WIFI_STATE_DISABLINGID);
	jfieldID WIFI_STATE_ENABLEDID = (*pEnv)->GetStaticFieldID(pEnv, WifiManagerCls, "WIFI_STATE_ENABLED", "I");
	int WIFI_STATE_ENABLED = (*pEnv)->GetStaticIntField(pEnv, WifiManagerCls, WIFI_STATE_ENABLEDID);
	jfieldID WIFI_STATE_ENABLINGID = (*pEnv)->GetStaticFieldID(pEnv, WifiManagerCls, "WIFI_STATE_ENABLING", "I");
	int WIFI_STATE_ENABLING = (*pEnv)->GetStaticIntField(pEnv, WifiManagerCls, WIFI_STATE_ENABLINGID);
	jfieldID WIFI_STATE_UNKNOWNID = (*pEnv)->GetStaticFieldID(pEnv, WifiManagerCls, "WIFI_STATE_UNKNOWN", "I");
	int WIFI_STATE_UNKNOWN = (*pEnv)->GetStaticIntField(pEnv, WifiManagerCls, WIFI_STATE_UNKNOWNID);

	jobject connectionInfo = (*pEnv)->CallObjectMethod(pEnv, wifiManager, getConnectionInfoMID);
	if (strcmp(action, SCAN_RESULTS_AVAILABLE_ACTION)) {
	} else if (strcmp(action, WIFI_STATE_CHANGED_ACTION)) {
		jmethodID getIntExtraMID = (*pEnv)->GetMethodID(pEnv, intentCls, "getIntExtra", "(Ljava/lang/String;I)I");
		int extraWifiState = (*pEnv)->CallIntMethod(pEnv, pIntent, EXTRA_WIFI_STATE, WIFI_STATE_UNKNOWN);

		if (extraWifiState == WIFI_STATE_DISABLED) {
			(*pEnv)->SetIntField(pEnv, pObj, isEnabledID, 0);
			(*pEnv)->SetIntField(pEnv, pObj, isConnectedID, 0);
			(*pEnv)->SetObjectField(pEnv, pObj, wifiInfoID, connectionInfo);
		} else if (extraWifiState == WIFI_STATE_DISABLING) {
			(*pEnv)->SetIntField(pEnv, pObj, isEnabledID, 0);
			(*pEnv)->SetIntField(pEnv, pObj, isConnectedID, 0);
			(*pEnv)->SetObjectField(pEnv, pObj, wifiInfoID, connectionInfo);
		} else if (extraWifiState == WIFI_STATE_ENABLED) {
			(*pEnv)->SetIntField(pEnv, pObj, isEnabledID, 1);
			(*pEnv)->SetObjectField(pEnv, pObj, wifiInfoID, connectionInfo);
		} else if (extraWifiState == WIFI_STATE_ENABLING) {
			(*pEnv)->SetIntField(pEnv, pObj, isEnabledID, 1);
			(*pEnv)->SetObjectField(pEnv, pObj, wifiInfoID, connectionInfo);
		} else if (extraWifiState == WIFI_STATE_UNKNOWN) {
			(*pEnv)->SetIntField(pEnv, pObj, isEnabledID, -1);
			(*pEnv)->SetObjectField(pEnv, pObj, wifiInfoID, connectionInfo);
		}
	} else if (strcmp(action, NETWORK_STATE_CHANGED_ACTION)) {
		jmethodID getParcelableExtraMID = (*pEnv)->GetMethodID(pEnv, intentCls, "getParcelableExtra", "(Ljava/lang/String;)Landroid/net/NetworkInfo;");
		jobject parcelableExtra = (*pEnv)->CallObjectMethod(pEnv, pIntent, getParcelableExtraMID);
		jclass NetworkInfoCls = (*pEnv)->FindClass(pEnv, "android/net/NetworkInfo");
		jmethodID getDetailedStateMID = (*pEnv)->GetMethodID(pEnv, NetworkInfoCls, "getDetailedState", "()Landroid/net/NetworkInfo$DetailedState;");
		jobject aState = (*pEnv)->CallObjectMethod(pEnv, parcelableExtra, getDetailedStateMID);

		//TODO: updateIsConnected(state);
		jclass DetailedStateCls = (*pEnv)->FindClass(pEnv, "android/net/NetworkInfo$DetailedState");
		jfieldID SCANNINGID = (*pEnv)->GetStaticFieldID(pEnv, DetailedStateCls, "SCANNING", "Landroid/net/NetworkInfo$DetailedState;");
		jobject SCANNING = (*pEnv)->GetStaticObjectField(pEnv, DetailedStateCls, SCANNINGID);
		jfieldID CONNECTINGID = (*pEnv)->GetStaticFieldID(pEnv, DetailedStateCls, "CONNECTING", "Landroid/net/NetworkInfo$DetailedState;");
		jobject CONNECTING = (*pEnv)->GetStaticObjectField(pEnv, DetailedStateCls, CONNECTINGID);
		jfieldID OBTAINING_IPADDRID = (*pEnv)->GetStaticFieldID(pEnv, DetailedStateCls, "OBTAINING_IPADDR", "Landroid/net/NetworkInfo$DetailedState;");
		jobject OBTAINING_IPADDR = (*pEnv)->GetStaticObjectField(pEnv, DetailedStateCls, OBTAINING_IPADDR);
		jfieldID CONNECTEDID = (*pEnv)->GetStaticFieldID(pEnv, DetailedStateCls, "CONNECTED", "Landroid/net/NetworkInfo$DetailedState;");
		jobject CONNECTED = (*pEnv)->GetStaticObjectField(pEnv, DetailedStateCls, CONNECTEDID);
		jfieldID DISCONNECTINGID = (*pEnv)->GetStaticFieldID(pEnv, DetailedStateCls, "DISCONNECTING", "Landroid/net/NetworkInfo$DetailedState;");
		jobject DISCONNECTING = (*pEnv)->GetStaticObjectField(pEnv, DetailedStateCls, DISCONNECTINGID);
		jfieldID DISCONNECTEDID = (*pEnv)->GetStaticFieldID(pEnv, DetailedStateCls, "DISCONNECTED", "Landroid/net/NetworkInfo$DetailedState;");
		jobject DISCONNECTED = (*pEnv)->GetStaticObjectField(pEnv, DetailedStateCls, DISCONNECTEDID);
		jfieldID FAILEDID = (*pEnv)->GetStaticFieldID(pEnv, DetailedStateCls, "FAILED", "Landroid/net/NetworkInfo$DetailedState;");
		jobject FAILED = (*pEnv)->GetStaticObjectField(pEnv, DetailedStateCls, FAILEDID);

		jclass WifiSenseReceiverCls = (*pEnv)->GetObjectClass(pEnv, pObj);
		jfieldID isConnectedID = (*pEnv)->GetFieldID(pEnv, WifiSenseReceiverCls, "isConnected", "I");

		if (aState==SCANNING)
			(*pEnv)->SetIntField(pEnv, pObj, isConnectedID, 0);
		else if (aState==CONNECTING)
			(*pEnv)->SetIntField(pEnv, pObj, isConnectedID, 0);
		else if (aState==OBTAINING_IPADDR)
			(*pEnv)->SetIntField(pEnv, pObj, isConnectedID, 0);
		else if (aState==CONNECTED)
			(*pEnv)->SetIntField(pEnv, pObj, isConnectedID, 1);
		else if (aState==DISCONNECTING)
			(*pEnv)->SetIntField(pEnv, pObj, isConnectedID, 0);
		else if (aState==DISCONNECTED)
			(*pEnv)->SetIntField(pEnv, pObj, isConnectedID, 0);
		else if (aState==FAILED)
			(*pEnv)->SetIntField(pEnv, pObj, isConnectedID, 0);

		(*pEnv)->SetObjectField(pEnv, pObj, wifiInfoID, connectionInfo);
	}
}

JNIEXPORT void JNICALL Java_org_morphone_sense_wifi_WifiSenseReceiver_updateIsConnectedNative
  (JNIEnv *pEnv, jobject pObj, jobject aState) {
	jclass DetailedStateCls = (*pEnv)->FindClass(pEnv, "android/net/NetworkInfo$DetailedState");
	jfieldID SCANNINGID = (*pEnv)->GetStaticFieldID(pEnv, DetailedStateCls, "SCANNING", "Landroid/net/NetworkInfo$DetailedState;");
	jobject SCANNING = (*pEnv)->GetStaticObjectField(pEnv, DetailedStateCls, SCANNINGID);
	jfieldID CONNECTINGID = (*pEnv)->GetStaticFieldID(pEnv, DetailedStateCls, "CONNECTING", "Landroid/net/NetworkInfo$DetailedState;");
	jobject CONNECTING = (*pEnv)->GetStaticObjectField(pEnv, DetailedStateCls, CONNECTINGID);
	jfieldID OBTAINING_IPADDRID = (*pEnv)->GetStaticFieldID(pEnv, DetailedStateCls, "OBTAINING_IPADDR", "Landroid/net/NetworkInfo$DetailedState;");
	jobject OBTAINING_IPADDR = (*pEnv)->GetStaticObjectField(pEnv, DetailedStateCls, OBTAINING_IPADDR);
	jfieldID CONNECTEDID = (*pEnv)->GetStaticFieldID(pEnv, DetailedStateCls, "CONNECTED", "Landroid/net/NetworkInfo$DetailedState;");
	jobject CONNECTED = (*pEnv)->GetStaticObjectField(pEnv, DetailedStateCls, CONNECTEDID);
	jfieldID DISCONNECTINGID = (*pEnv)->GetStaticFieldID(pEnv, DetailedStateCls, "DISCONNECTING", "Landroid/net/NetworkInfo$DetailedState;");
	jobject DISCONNECTING = (*pEnv)->GetStaticObjectField(pEnv, DetailedStateCls, DISCONNECTINGID);
	jfieldID DISCONNECTEDID = (*pEnv)->GetStaticFieldID(pEnv, DetailedStateCls, "DISCONNECTED", "Landroid/net/NetworkInfo$DetailedState;");
	jobject DISCONNECTED = (*pEnv)->GetStaticObjectField(pEnv, DetailedStateCls, DISCONNECTEDID);
	jfieldID FAILEDID = (*pEnv)->GetStaticFieldID(pEnv, DetailedStateCls, "FAILED", "Landroid/net/NetworkInfo$DetailedState;");
	jobject FAILED = (*pEnv)->GetStaticObjectField(pEnv, DetailedStateCls, FAILEDID);

	jclass WifiSenseReceiverCls = (*pEnv)->GetObjectClass(pEnv, pObj);
	jfieldID isConnectedID = (*pEnv)->GetFieldID(pEnv, WifiSenseReceiverCls, "isConnected", "I");

	if (aState==SCANNING)
		(*pEnv)->SetIntField(pEnv, pObj, isConnectedID, 0);
	else if (aState==CONNECTING)
		(*pEnv)->SetIntField(pEnv, pObj, isConnectedID, 0);
	else if (aState==OBTAINING_IPADDR)
		(*pEnv)->SetIntField(pEnv, pObj, isConnectedID, 0);
	else if (aState==CONNECTED)
		(*pEnv)->SetIntField(pEnv, pObj, isConnectedID, 1);
	else if (aState==DISCONNECTING)
		(*pEnv)->SetIntField(pEnv, pObj, isConnectedID, 0);
	else if (aState==DISCONNECTED)
		(*pEnv)->SetIntField(pEnv, pObj, isConnectedID, 0);
	else if (aState==FAILED)
		(*pEnv)->SetIntField(pEnv, pObj, isConnectedID, 0);

}
