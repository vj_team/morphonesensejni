#include <jni.h>
#include <stdio.h>
#include <android/log.h>

#include <utilities.h>
#include <BatterySense.h>

JNIEXPORT jboolean JNICALL Java_org_morphone_sense_battery_BatterySense_isChargingNative
  (JNIEnv *pEnv, jobject pObj) {

	jclass thisCls = (*pEnv)->GetObjectClass(pEnv, pObj);

	jfieldID chargeOnID = (*pEnv)->GetFieldID(pEnv, thisCls, "charge_on", "I");
	jint chargeOn = (*pEnv)->GetIntField(pEnv, pObj, chargeOnID);
	if (chargeOn < 0) {
		throwBatterySenseException(pEnv, (*pEnv)->NewStringUTF(pEnv,"Error while getting charge_on"));
		return;
	}
	else if (chargeOn == 0)
		return 0;
	else
		return 1;
}

JNIEXPORT jint JNICALL Java_org_morphone_sense_battery_BatterySense_getPercentageNative
  (JNIEnv *pEnv, jobject pObj) {

	jclass thisCls = (*pEnv)->GetObjectClass(pEnv, pObj);

	jfieldID percentageID = (*pEnv)->GetFieldID(pEnv, thisCls, "percentage", "I");
	jint percentage = (*pEnv)->GetIntField(pEnv, pObj, percentageID);

	if (percentage < 0){
		throwBatterySenseException(pEnv, (*pEnv)->NewStringUTF(pEnv,"Error while getting percentage"));
		return;
	}
	else
		return percentage;
}

JNIEXPORT jint JNICALL Java_org_morphone_sense_battery_BatterySense_getTemperatureNative
  (JNIEnv *pEnv, jobject pObj) {

	jclass thisCls = (*pEnv)->GetObjectClass(pEnv, pObj);

	jfieldID temperatureID = (*pEnv)->GetFieldID(pEnv, thisCls, "temperature", "I");
	jint temperature = (*pEnv)->GetIntField(pEnv, pObj, temperatureID);

	if (temperature < 0) {
		throwBatterySenseException(pEnv, (*pEnv)->NewStringUTF(pEnv,"Error while getting temperature"));
		return;
	}
	else
		return temperature;
}

JNIEXPORT jint JNICALL Java_org_morphone_sense_battery_BatterySense_getVoltageNative
  (JNIEnv *pEnv, jobject pObj) {

	jclass thisCls = (*pEnv)->GetObjectClass(pEnv, pObj);

	jfieldID voltageID = (*pEnv)->GetFieldID(pEnv, thisCls, "voltage", "I");
	jint voltage = (*pEnv)->GetIntField(pEnv, pObj, voltageID);

	if (voltage < 0) {
		throwBatterySenseException(pEnv, (*pEnv)->NewStringUTF(pEnv,"Error while getting voltage"));
		return;
	}
	else
		return voltage;
}

JNIEXPORT jstring JNICALL Java_org_morphone_sense_battery_BatterySense_getTechnologyNative
  (JNIEnv *pEnv, jobject pObj) {

	jclass thisCls = (*pEnv)->GetObjectClass(pEnv, pObj);

	jfieldID technologyID = (*pEnv)->GetFieldID(pEnv, thisCls, "technology", "Ljava/lang/String;");
	jobject strTechnology = (*pEnv)->GetObjectField(pEnv, pObj, technologyID);
	const char* str = (*pEnv)->GetStringUTFChars(pEnv, strTechnology, NULL);
	if (str == NULL) {
		throwBatterySenseException(pEnv, (*pEnv)->NewStringUTF(pEnv,"Error while getting technology"));
		return;
	} else {	
		jstring technology = (*pEnv)->NewStringUTF(pEnv, str);
		(*pEnv)->ReleaseStringUTFChars(pEnv, strTechnology, str);

		return technology;
	}
}


JNIEXPORT jint JNICALL Java_org_morphone_sense_battery_BatterySense_getHealthNative
  (JNIEnv *pEnv, jobject pObj) {

	jclass thisCls = (*pEnv)->GetObjectClass(pEnv, pObj);

	jfieldID healthID = (*pEnv)->GetFieldID(pEnv, thisCls, "health", "I");
	jint health = (*pEnv)->GetIntField(pEnv, pObj, healthID);

	if (health < 0){
		throwBatterySenseException(pEnv, (*pEnv)->NewStringUTF(pEnv,"Error while getting health"));
		return;
	}
	else
		return health;
}

JNIEXPORT void JNICALL Java_org_morphone_sense_battery_BatterySense_onReceiveNative
  (JNIEnv *pEnv, jobject pObj, jobject pContext, jobject pIntent) {

	jclass thisCls = (*pEnv)->GetObjectClass(pEnv, pObj);

	jclass contextCls = (*pEnv)->GetObjectClass(pEnv, pContext);

	jclass intentCls = (*pEnv)->GetObjectClass(pEnv, pIntent);
	jfieldID charge_on_prevID = (*pEnv)->GetFieldID(pEnv, thisCls, "charge_on_prev", "I");
	jint charge_on_prev = (*pEnv)->GetIntField(pEnv, pObj, charge_on_prevID);

	jfieldID charge_onID = (*pEnv)->GetFieldID(pEnv, thisCls, "charge_on", "I");
	jint charge_on = (*pEnv)->GetIntField(pEnv, pObj, charge_onID);

	jfieldID healthID = (*pEnv)->GetFieldID(pEnv, thisCls, "health", "I");
	jint health = (*pEnv)->GetIntField(pEnv, pObj, healthID);

	charge_on_prev = charge_on;

	jmethodID getIntExtraMID = (*pEnv)->GetMethodID(pEnv, intentCls, "getIntExtra", "(Ljava/lang/String;I)I");

	jclass batteryManagerCls = (*pEnv)->FindClass(pEnv, "android/os/BatteryManager");

	// get value of string BatteryManager.EXTRA_LEVEL
	jfieldID bmEXTRA_LEVELID = (*pEnv)->GetStaticFieldID(pEnv, batteryManagerCls, "EXTRA_LEVEL", "Ljava/lang/String;");
	jstring bmEXTRA_LEVEL = (jstring)(*pEnv)->GetStaticObjectField(pEnv, batteryManagerCls, bmEXTRA_LEVELID);
	// get value of string BatteryManager.EXTRA_SCALE
	jfieldID bmEXTRA_SCALEID = (*pEnv)->GetStaticFieldID(pEnv, batteryManagerCls, "EXTRA_SCALE", "Ljava/lang/String;");
	jstring bmEXTRA_SCALE = (jstring)(*pEnv)->GetStaticObjectField(pEnv, batteryManagerCls, bmEXTRA_SCALEID);
	// get value of string BatteryManager.EXTRA_PLUGGED
	jfieldID bmEXTRA_PLUGGEDID = (*pEnv)->GetStaticFieldID(pEnv, batteryManagerCls, "EXTRA_PLUGGED", "Ljava/lang/String;");
	jstring bmEXTRA_PLUGGED = (jstring)(*pEnv)->GetStaticObjectField(pEnv, batteryManagerCls, bmEXTRA_PLUGGEDID);
	// get value of string BatteryManager.EXTRA_HEALTH
	jfieldID bmEXTRA_HEALTHID = (*pEnv)->GetStaticFieldID(pEnv, batteryManagerCls, "EXTRA_HEALTH", "Ljava/lang/String;");
	jstring bmEXTRA_HEALTH = (jstring)(*pEnv)->GetStaticObjectField(pEnv, batteryManagerCls, bmEXTRA_HEALTHID);
	// get value of string BatteryManager.EXTRA_TECHNOLOGY
	jfieldID bmEXTRA_TECHNOLOGYID = (*pEnv)->GetStaticFieldID(pEnv, batteryManagerCls, "EXTRA_TECHNOLOGY", "Ljava/lang/String;");
	jstring bmEXTRA_TECHNOLOGY = (jstring)(*pEnv)->GetStaticObjectField(pEnv, batteryManagerCls, bmEXTRA_TECHNOLOGYID);
	// get value of string BatteryManager.EXTRA_TEMPERATURE
	jfieldID bmEXTRA_TEMPERATUREID = (*pEnv)->GetStaticFieldID(pEnv, batteryManagerCls, "EXTRA_TEMPERATURE", "Ljava/lang/String;");
	jstring bmEXTRA_TEMPERATURE = (jstring)(*pEnv)->GetStaticObjectField(pEnv, batteryManagerCls, bmEXTRA_TEMPERATUREID);
	// get value of string BatteryManager.EXTRA_VOLTAGE
	jfieldID bmEXTRA_VOLTAGEID = (*pEnv)->GetStaticFieldID(pEnv, batteryManagerCls, "EXTRA_VOLTAGE", "Ljava/lang/String;");
	jstring bmEXTRA_VOLTAGE = (jstring)(*pEnv)->GetStaticObjectField(pEnv, batteryManagerCls, bmEXTRA_VOLTAGEID);

	// get value of int BatteryManager.BATTERY_STATUS_UNKNOWN
	jfieldID bmBATTERY_STATUS_UNKNOWNID = (*pEnv)->GetStaticFieldID(pEnv, batteryManagerCls, "BATTERY_STATUS_UNKNOWN", "I");
	jint bmBATTERY_STATUS_UNKNOWNi = (*pEnv)->GetStaticIntField(pEnv, batteryManagerCls, bmBATTERY_STATUS_UNKNOWNID);
	// get value of int BatteryManager.BATTERY_STATUS_FULL
	jfieldID bmBATTERY_STATUS_FULLID = (*pEnv)->GetStaticFieldID(pEnv, batteryManagerCls, "BATTERY_STATUS_FULL", "I");
	jint bmBATTERY_STATUS_FULL = (*pEnv)->GetStaticIntField(pEnv, batteryManagerCls, bmBATTERY_STATUS_FULLID);
	// get value of int BatteryManager.BATTERY_STATUS_DISCHARGING
	jfieldID bmBATTERY_STATUS_DISCHARGINGID = (*pEnv)->GetStaticFieldID(pEnv, batteryManagerCls, "BATTERY_STATUS_DISCHARGING", "I");
	jint bmBATTERY_STATUS_DISCHARGING = (*pEnv)->GetStaticIntField(pEnv, batteryManagerCls, bmBATTERY_STATUS_DISCHARGINGID);
	// get value of int BatteryManager.BATTERY_STATUS_NOT_CHARGING
	jfieldID bmBATTERY_STATUS_NOT_CHARGINGID = (*pEnv)->GetStaticFieldID(pEnv, batteryManagerCls, "BATTERY_STATUS_NOT_CHARGING", "I");
	jint bmBATTERY_STATUS_NOT_CHARGING = (*pEnv)->GetStaticIntField(pEnv, batteryManagerCls, bmBATTERY_STATUS_NOT_CHARGINGID);
	// get value of int BatteryManager.BATTERY_PLUGGED_AC
	jfieldID bmBATTERY_PLUGGED_ACID = (*pEnv)->GetStaticFieldID(pEnv, batteryManagerCls, "BATTERY_PLUGGED_AC", "I");
	jint bmBATTERY_PLUGGED_AC = (*pEnv)->GetStaticIntField(pEnv, batteryManagerCls, bmBATTERY_PLUGGED_ACID);
	// get value of int BatteryManager.BATTERY_PLUGGED_USB
	jfieldID bmBATTERY_PLUGGED_USBID = (*pEnv)->GetStaticFieldID(pEnv, batteryManagerCls, "BATTERY_PLUGGED_USB", "I");
	jint bmBATTERY_PLUGGED_USB = (*pEnv)->GetStaticIntField(pEnv, batteryManagerCls, bmBATTERY_PLUGGED_USBID);

	int rawLevel = (*pEnv)->CallIntMethod(pEnv, pIntent, getIntExtraMID, bmEXTRA_LEVEL, -1);
	int scale = (*pEnv)->CallIntMethod(pEnv, pIntent, getIntExtraMID, bmEXTRA_SCALE, -1);

	charge_on = (*pEnv)->CallIntMethod(pEnv, pIntent, getIntExtraMID, bmEXTRA_PLUGGED, -1);
	health = (*pEnv)->CallIntMethod(pEnv, pIntent, getIntExtraMID, bmEXTRA_HEALTH, -1);

	jstring technology = "";
	jmethodID getExtrasMID = (*pEnv)->GetMethodID(pEnv, intentCls, "getExtras", "()Landroid/os/Bundle;");
	jobject retGetExtras = (jobject)(*pEnv)->CallObjectMethod(pEnv, pIntent, getExtrasMID);
	if (retGetExtras == NULL)
		technology = "n/a";
	else {
		jclass bundleCls = (*pEnv)->FindClass(pEnv, "android/os/Bundle");
		jmethodID bundleGetStringMID = (*pEnv)->GetMethodID(pEnv, bundleCls, "getString", "(Ljava/lang/String;)Ljava/lang/String;");

		technology = (*pEnv)->CallObjectMethod(pEnv, retGetExtras, bundleGetStringMID, bmEXTRA_TECHNOLOGY);
	}

	// parse to jstring to call getIntExtra()
	char str[1];
	sprintf(str, "%d", bmBATTERY_STATUS_UNKNOWNi);
	jstring bmBATTERY_STATUS_UNKNOWN = (*pEnv)->NewStringUTF(pEnv, str);

	int status = -1;
	status = (*pEnv)->CallIntMethod(pEnv, pIntent, getIntExtraMID, bmBATTERY_STATUS_UNKNOWN, -1);

	jint percentage = -1;
	jint status_prev = -1;
	if (rawLevel>=0 && scale>0) {
		percentage = (rawLevel*100)/scale;
	}

	jmethodID intentConstructorID = (*pEnv)->GetMethodID(pEnv, intentCls, "<init>", "(Ljava/lang/String;)V");
	jobject i;
	if (status!=status_prev && status_prev!=1) {
		if (status_prev==bmBATTERY_STATUS_FULL &&
				(status==bmBATTERY_STATUS_DISCHARGING ||
				status==bmBATTERY_STATUS_NOT_CHARGING)
				&& percentage==100) {
			i = (*pEnv)->NewObject(pEnv, intentCls, intentConstructorID, "message");
			jmethodID intentPutExtraID = (*pEnv)->GetMethodID(pEnv, intentCls, "putExtra", "(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;");
			(*pEnv)->CallObjectMethod(pEnv, i, intentPutExtraID, "type", "msg_battery_full");

			jmethodID contextSendBroadcast = (*pEnv)->GetMethodID(pEnv, contextCls, "sendBroadcast", "(Landroid/content/Intent;)V");
			(*pEnv)->CallVoidMethod(pEnv, pContext, contextSendBroadcast, i);
		}
	}
	status_prev = status;

	if (charge_on==bmBATTERY_PLUGGED_AC || charge_on==bmBATTERY_PLUGGED_USB
			&& charge_on_prev!=bmBATTERY_PLUGGED_AC && charge_on_prev!=bmBATTERY_PLUGGED_USB) {
		i = (*pEnv)->NewObject(pEnv, intentCls, intentConstructorID, "message");
		jmethodID intentPutExtraID = (*pEnv)->GetMethodID(pEnv, intentCls, "putExtra", "(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;");
		(*pEnv)->CallObjectMethod(pEnv, i, intentPutExtraID, "type", "msg_plugged_on");

		jmethodID contextSendBroadcast = (*pEnv)->GetMethodID(pEnv, contextCls, "sendBroadcast", "(Landroid/content/Intent;)V");
		(*pEnv)->CallVoidMethod(pEnv, pContext, contextSendBroadcast, i);
	}
	jint temperature = (*pEnv)->CallIntMethod(pEnv, pIntent, getIntExtraMID, bmEXTRA_TEMPERATURE, -1);
	jint vontage = (*pEnv)->CallIntMethod(pEnv, pIntent, getIntExtraMID, bmEXTRA_VOLTAGE, -1);

}

void throwBatterySenseException(JNIEnv *pEnv, jstring eMsg) {
	jclass BatterySenseExceptionCls = (*pEnv)->FindClass(pEnv, "org/morphone/sense/battery/BatterySenseException");
	if (BatterySenseExceptionCls!=NULL) 
		(*pEnv)->ThrowNew(pEnv, BatterySenseExceptionCls, eMsg);
	(*pEnv)->DeleteLocalRef(pEnv, BatterySenseExceptionCls);
}

