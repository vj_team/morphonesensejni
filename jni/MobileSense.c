#include <jni.h>
#include <stdio.h>
#include <android/log.h>

#include <utilities.h>
#include <MobileSense.h>

JNIEXPORT jstring JNICALL Java_org_morphone_sense_mobile_MobileSense_getIMEINative
  (JNIEnv *pEnv, jobject pObj) {
  	jclass MobileSenseCls = (*pEnv)->GetObjectClass(pEnv, pObj);

  	jfieldID IMEIID = (*pEnv)->GetFieldID(pEnv, MobileSenseCls, "IMEI", "Ljava/lang/String;");
  	jstring IMEI = (jstring)(*pEnv)->GetObjectField(pEnv, pObj, IMEIID);
  	if (IMEI!=NULL)
  		return IMEI;
    else 
      throwMobileSenseException(pEnv, (*pEnv)->NewStringUTF(pEnv,"IMEI not available for this device"));
}

JNIEXPORT jint JNICALL Java_org_morphone_sense_mobile_MobileSense_getCallStateNative
  (JNIEnv *pEnv, jobject pObj) {
  	jclass MobileSenseCls = (*pEnv)->GetObjectClass(pEnv, pObj);

  	jfieldID call_stateID = (*pEnv)->GetFieldID(pEnv, MobileSenseCls, "call_state", "I");
  	int call_state = (*pEnv)->GetIntField(pEnv, pObj, call_stateID);
  	if (call_state>=0) 
  		return call_state;
    else
      throwMobileSenseException(pEnv, (*pEnv)->NewStringUTF(pEnv,"Error while getting callState"));
}

JNIEXPORT jlong JNICALL Java_org_morphone_sense_mobile_MobileSense_getRxBytesNative
  (JNIEnv *pEnv, jobject pObj) {
  	jclass TrafficStatsCls = (*pEnv)->FindClass(pEnv, "android/net/TrafficStats");

  	jfieldID UNSUPPORTEDID = (*pEnv)->GetStaticFieldID(pEnv, TrafficStatsCls, "UNSUPPORTED", "I");
  	int UNSUPPORTED = (*pEnv)->GetStaticIntField(pEnv, TrafficStatsCls, UNSUPPORTEDID);
    

  	long rxBytes = UNSUPPORTED;

  	jmethodID getMobileRxBytesMID = (*pEnv)->GetStaticMethodID(pEnv, TrafficStatsCls, "getMobileRxBytes", "()L");
  	rxBytes = (*pEnv)->CallStaticFloatMethod(pEnv, TrafficStatsCls, getMobileRxBytesMID);
    if((*pEnv)->ExceptionOccurred(pEnv)) {
      throwMobileSenseException(pEnv, (*pEnv)->NewStringUTF(pEnv,"Error while getting rxBytes"));
    }

  	if (rxBytes!=UNSUPPORTED)
  		return rxBytes;
    else
      throwMobileSenseException(pEnv, (*pEnv)->NewStringUTF(pEnv,"The device does not support the statistic: rxBytes"));
}

JNIEXPORT jlong JNICALL Java_org_morphone_sense_mobile_MobileSense_getTxBytesNative
  (JNIEnv *pEnv, jobject pObj) {
  	jclass TrafficStatsCls = (*pEnv)->FindClass(pEnv, "android/net/TrafficStats");

  	jfieldID UNSUPPORTEDID = (*pEnv)->GetStaticFieldID(pEnv, TrafficStatsCls, "UNSUPPORTED", "I");
  	int UNSUPPORTED = (*pEnv)->GetStaticIntField(pEnv, TrafficStatsCls, UNSUPPORTEDID);

  	long txBytes = UNSUPPORTED;

  	jmethodID getMobileTxBytesMID = (*pEnv)->GetStaticMethodID(pEnv, TrafficStatsCls, "getMobileTxBytes", "()L");
  	txBytes = (*pEnv)->CallStaticFloatMethod(pEnv, TrafficStatsCls, getMobileTxBytesMID);
    if((*pEnv)->ExceptionOccurred(pEnv)) {
      throwMobileSenseException(pEnv, (*pEnv)->NewStringUTF(pEnv,"Error while getting txBytes"));
    }
  	if (txBytes!=UNSUPPORTED)
  		return txBytes;	
    else
      throwMobileSenseException(pEnv, (*pEnv)->NewStringUTF(pEnv,"The device does not support the statistic: txBytes"));
}

JNIEXPORT jint JNICALL Java_org_morphone_sense_mobile_MobileSense_getSignalStrengthNative
  (JNIEnv *pEnv, jobject pObj) {
	jclass MobileSenseCls = (*pEnv)->GetObjectClass(pEnv, pObj);

	jfieldID signal_strengthID = (*pEnv)->GetFieldID(pEnv, MobileSenseCls, "signal_strenght", "I");
	int signal_strength = (*pEnv)->GetIntField(pEnv, pObj, signal_strengthID);
	if(signal_strength>=0)
		return signal_strength;
  else
    throwMobileSenseException(pEnv, (*pEnv)->NewStringUTF(pEnv,"Error while getting signal strength"));
}

JNIEXPORT jint JNICALL Java_org_morphone_sense_mobile_MobileSense_getDataStateNative
  (JNIEnv *pEnv, jobject pObj) {
	jclass MobileSenseCls = (*pEnv)->GetObjectClass(pEnv, pObj);

	jfieldID data_stateID = (*pEnv)->GetFieldID(pEnv, MobileSenseCls, "data_state", "I");
	int data_state = (*pEnv)->GetIntField(pEnv, pObj, data_stateID);
	if(data_state>=0)
		return data_state;  	
  else
    throwMobileSenseException(pEnv, (*pEnv)->NewStringUTF(pEnv,"Error while getting data state"));
}

JNIEXPORT jint JNICALL Java_org_morphone_sense_mobile_MobileSense_getDataActivityNative
  (JNIEnv *pEnv, jobject pObj) {
	jclass MobileSenseCls = (*pEnv)->GetObjectClass(pEnv, pObj);

	jfieldID data_activityID = (*pEnv)->GetFieldID(pEnv, MobileSenseCls, "data_activity", "I");
	int data_activity = (*pEnv)->GetIntField(pEnv, pObj, data_activityID);
	if(data_activity>=0)
		return data_activity;  
  else 	
    throwMobileSenseException(pEnv, (*pEnv)->NewStringUTF(pEnv,"Error while getting data activity"));
}

JNIEXPORT jint JNICALL Java_org_morphone_sense_mobile_MobileSense_getNetworkTypeNative
  (JNIEnv *pEnv, jobject pObj) {
	jclass MobileSenseCls = (*pEnv)->GetObjectClass(pEnv, pObj);

	jfieldID network_typeID = (*pEnv)->GetFieldID(pEnv, MobileSenseCls, "network_type", "I");
	int network_type = (*pEnv)->GetIntField(pEnv, pObj, network_typeID);
	if(network_type>=0)
		return network_type;   
  else
    throwMobileSenseException(pEnv, (*pEnv)->NewStringUTF(pEnv,"Error while getting network type"));	
}

void throwMobileSenseException(JNIEnv *pEnv, jstring eMsg) {
  jclass MobileSenseExceptionCls = (*pEnv)->FindClass(pEnv, "org/morphone/sense/mobile/MobileSenseException");
  if (MobileSenseExceptionCls!=NULL) 
    (*pEnv)->ThrowNew(pEnv, MobileSenseExceptionCls, eMsg);
  (*pEnv)->DeleteLocalRef(pEnv, MobileSenseExceptionCls);
}