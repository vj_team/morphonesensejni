#include <jni.h>
#include <stdio.h>
#include <android/log.h>

#include <utilities.h>
#include <ScreenSenseReceiver.h>

JNIEXPORT jfloat JNICALL Java_org_morphone_sense_screen_ScreenSenseReceiver_getDisplayHeightNative
  (JNIEnv *pEnv, jobject pObj) {
  	jclass ScreenSenseReceiverCls = (*pEnv)->GetObjectClass(pEnv, pObj);

  	jfieldID displayHeightID = (*pEnv)->GetFieldID(pEnv, ScreenSenseReceiverCls, "displayHeight", "L");
	float displayHeight = (*pEnv)->GetFloatField(pEnv, pObj, displayHeightID);
	
	if (displayHeight>=0)
		return displayHeight;
	else
		throwScreenSenseException(pEnv, (*pEnv)->NewStringUTF(pEnv,"Error while getting displayHeight"));
}

JNIEXPORT jfloat JNICALL Java_org_morphone_sense_screen_ScreenSenseReceiver_getDisplayWidthNative
  (JNIEnv *pEnv, jobject pObj) {
  	jclass ScreenSenseReceiverCls = (*pEnv)->GetObjectClass(pEnv, pObj);

	jfieldID displayWidthID = (*pEnv)->GetFieldID(pEnv, ScreenSenseReceiverCls, "displayWidth", "L");
	float displayWidth = (*pEnv)->GetFloatField(pEnv, pObj, displayWidthID);

	if (displayWidth>=0)
		return displayWidth;
	else 	
		throwScreenSenseException(pEnv, (*pEnv)->NewStringUTF(pEnv,"Error while getting displayHeight"));
}

JNIEXPORT jfloat JNICALL Java_org_morphone_sense_screen_ScreenSenseReceiver_getDisplayRefreshRateNative
  (JNIEnv *pEnv, jobject pObj) {
  	jclass ScreenSenseReceiverCls = (*pEnv)->GetObjectClass(pEnv, pObj);

	jfieldID displayID = (*pEnv)->GetFieldID(pEnv, ScreenSenseReceiverCls, "display", "Landroid/view/Display;");
	jobject display = (*pEnv)->GetObjectField(pEnv, pObj, displayID);
	jclass DisplayCls = (*pEnv)->GetObjectClass(pEnv, display);
	jmethodID getRefreshRateMID = (*pEnv)->GetMethodID(pEnv, DisplayCls, "getRefreshRate", "()F");
	return (*pEnv)->CallFloatMethod(pEnv, display, getRefreshRateMID);
}

JNIEXPORT jfloat JNICALL Java_org_morphone_sense_screen_ScreenSenseReceiver_getOrientationNative
  (JNIEnv *pEnv, jobject pObj) {
	jclass ScreenSenseReceiverCls = (*pEnv)->GetObjectClass(pEnv, pObj);

	jfieldID displayID = (*pEnv)->GetFieldID(pEnv, ScreenSenseReceiverCls, "display", "Landroid/view/Display;");
	jobject display = (*pEnv)->GetObjectField(pEnv, pObj, displayID);
	jclass DisplayCls = (*pEnv)->GetObjectClass(pEnv, display);
	jmethodID getOrientationMID = (*pEnv)->GetMethodID(pEnv, DisplayCls, "getOrientation", "()I");
	return (*pEnv)->CallIntMethod(pEnv, display, getOrientationMID);  	
}	

JNIEXPORT jint JNICALL Java_org_morphone_sense_screen_ScreenSenseReceiver_getScreenBrightnessModelNative
  (JNIEnv *pEnv, jobject pObj) {
	jclass ScreenSenseReceiverCls = (*pEnv)->GetObjectClass(pEnv, pObj);

	jclass SystemCls = (*pEnv)->FindClass(pEnv, "android/provider/Settings$System");
	jmethodID getIntMID = (*pEnv)->GetStaticMethodID(pEnv, SystemCls, "getInt", "(Landroid/content/ContentResolver;Ljava/lang/String;)I");

	jfieldID contentResolverID = (*pEnv)->GetFieldID(pEnv, ScreenSenseReceiverCls, "contentResolver", "Landroid/content/ContentResolver;");
	jobject contentResolver = (*pEnv)->GetObjectField(pEnv, pObj, contentResolverID);
	jfieldID SCREEN_BRIGHTNESS_MODEID = (*pEnv)->GetStaticFieldID(pEnv, SystemCls, "SCREEN_BRIGHTNESS_MODE", "Ljava/lang/String;");
	jstring SCREEN_BRIGHTNESS_MODE = (*pEnv)->GetStaticObjectField(pEnv, SystemCls, SCREEN_BRIGHTNESS_MODEID);

	return (*pEnv)->CallStaticIntMethod(pEnv, SystemCls, getIntMID, contentResolver, SCREEN_BRIGHTNESS_MODE);  	
}

JNIEXPORT jboolean JNICALL Java_org_morphone_sense_screen_ScreenSenseReceiver_isScreenOnNative
  (JNIEnv *pEnv, jobject pObj) {
  	jclass ScreenSenseReceiverCls = (*pEnv)->GetObjectClass(pEnv, pObj);

  	jfieldID screenOnID = (*pEnv)->GetFieldID(pEnv, ScreenSenseReceiverCls, "screenOn", "I");
  	int screenOn = (*pEnv)->GetIntField(pEnv, pObj, screenOnID);

  	if (screenOn==1)
  		return 1;
  	else if (screenOn==0)
  		return 0;
}

JNIEXPORT jint JNICALL Java_org_morphone_sense_screen_ScreenSenseReceiver_getScreenBrightnessNative
  (JNIEnv *pEnv, jobject pObj) {
  	jclass ScreenSenseReceiverCls = (*pEnv)->GetObjectClass(pEnv, pObj);

  	jfieldID screenOnID = (*pEnv)->GetFieldID(pEnv, ScreenSenseReceiverCls, "screenOn", "I");
  	int screenOn = (*pEnv)->GetIntField(pEnv, pObj, screenOnID);	

  	if (screenOn < 1)
  		return 0;
  	else {
  		jclass SystemCls = (*pEnv)->FindClass(pEnv, "android/provider/Settings$System");
		jmethodID getIntMID = (*pEnv)->GetStaticMethodID(pEnv, SystemCls, "getInt", "(Landroid/content/ContentResolver;Ljava/lang/String;)I");

		jfieldID contentResolverID = (*pEnv)->GetFieldID(pEnv, ScreenSenseReceiverCls, "contentResolver", "Landroid/content/ContentResolver;");
		jobject contentResolver = (*pEnv)->GetObjectField(pEnv, pObj, contentResolverID);
		jfieldID SCREEN_BRIGHTNESS_MODEID = (*pEnv)->GetStaticFieldID(pEnv, SystemCls, "SCREEN_BRIGHTNESS_MODE", "Ljava/lang/String;");
		jstring SCREEN_BRIGHTNESS_MODE = (*pEnv)->GetStaticObjectField(pEnv, SystemCls, SCREEN_BRIGHTNESS_MODEID);

		int currentBrightnessMode = (*pEnv)->CallStaticIntMethod(pEnv, SystemCls, getIntMID, contentResolver, SCREEN_BRIGHTNESS_MODE); 	
		jfieldID SCREEN_BRIGHTNESS_MODE_MANUALID = (*pEnv)->GetStaticFieldID(pEnv, SystemCls, "SCREEN_BRIGHTNESS_MODE_MANUAL", "I");
		int SCREEN_BRIGHTNESS_MODE_MANUAL = (*pEnv)->GetStaticIntField(pEnv, SystemCls, SCREEN_BRIGHTNESS_MODE_MANUALID);

		if (currentBrightnessMode==SCREEN_BRIGHTNESS_MODE_MANUAL) {
			__android_log_print(ANDROID_LOG_INFO, "Morphone", "BRIGHTNESS_MODE_MANUAL");
			jfieldID SCREEN_BRIGHTNESSID = (*pEnv)->GetStaticFieldID(pEnv, SystemCls, "SCREEN_BRIGHTNESS", "Ljava/lang/String;");
			jstring SCREEN_BRIGHTNESS = (jstring)(*pEnv)->GetStaticObjectField(pEnv, SystemCls, SCREEN_BRIGHTNESSID);

			return (*pEnv)->CallStaticIntMethod(pEnv, SystemCls, getIntMID, contentResolver, SCREEN_BRIGHTNESS);
		} else {
			__android_log_print(ANDROID_LOG_INFO, "Morphone", "BRIGHTNESS_MODE_AUTO");
			jfieldID brightness_pathID = (*pEnv)->GetFieldID(pEnv, ScreenSenseReceiverCls, "brightness_path", "Ljava/lang/String;");
			jstring brightness_path = (*pEnv)->GetObjectField(pEnv, pObj, brightness_pathID);

			if (brightness_path!=NULL) {
				//TODO return getValue(brightness_path);
				int sz = 8*24;
				char line[sz];
				FILE* fp = fopen(brightness_path, "r");

			//	if (fp!=NULL) {
				if (fp == NULL){
					return -1;
				}
				else {
					fgets(line, 100, fp);
			//		fflush(fp);

			//	} else {
			//		fclose(fp);
			//	}
				int ret = atoi(line);
				fclose(fp);
				if (ret!=NULL)
					return ret;

				}
			}
		}
  	}
}

JNIEXPORT void JNICALL Java_org_morphone_sense_screen_ScreenSenseReceiver_onReceiveNative
  (JNIEnv *pEnv, jobject pObj, jobject pContext, jobject pIntent) {
  	jclass ScreenSenseReceiverCls = (*pEnv)->GetObjectClass(pEnv, pObj);
  	jclass IntentCls = (*pEnv)->GetObjectClass(pEnv, pIntent);
  	jmethodID getActionMID = (*pEnv)->GetMethodID(pEnv, IntentCls, "getAction", "()Ljava/lang/String;");
  	jstring action = (jstring)(*pEnv)->CallObjectMethod(pEnv, pIntent, getActionMID);

  	jfieldID ACTION_SCREEN_OFFID = (*pEnv)->GetStaticFieldID(pEnv, IntentCls, "ACTION_SCREEN_OFF", "Ljava/lang/String;");
  	jstring ACTION_SCREEN_OFF = (*pEnv)->GetStaticObjectField(pEnv, IntentCls, ACTION_SCREEN_OFFID);
  	jfieldID ACTION_SCREEN_ONID = (*pEnv)->GetStaticFieldID(pEnv, IntentCls, "ACTION_SCREEN_ON", "Ljava/lang/String;");
  	jstring ACTION_SCREEN_ON = (*pEnv)->GetStaticObjectField(pEnv, IntentCls, ACTION_SCREEN_ONID);  	

  	jfieldID screenOnID = (*pEnv)->GetFieldID(pEnv, ScreenSenseReceiverCls, "screenOn", "I");

  	if (action==ACTION_SCREEN_OFF) {
  		(*pEnv)->SetIntField(pEnv, pObj, screenOnID, 0);
  		__android_log_print(ANDROID_LOG_INFO, "Morphone", "Set screenOn = 0. %d",(*pEnv)->GetIntField(pEnv, pObj, screenOnID));
  	}
  	else if (action==ACTION_SCREEN_ON) {
  		(*pEnv)->SetIntField(pEnv, pObj, screenOnID, 1);
  		__android_log_print(ANDROID_LOG_INFO, "Morphone", "Set screenOn = 1. %d",(*pEnv)->GetIntField(pEnv, pObj, screenOnID));
  	}
  	else {
  		jfieldID powerManagerID = (*pEnv)->GetFieldID(pEnv, ScreenSenseReceiverCls, "powerManager", "Landroid/os/PowerManager;");
  		jobject powerManager = (*pEnv)->GetObjectField(pEnv, pObj, powerManagerID);
  		jclass PowerManagerCls = (*pEnv)->GetObjectClass(pEnv, powerManager);
  		jmethodID isScreenOnMID = (*pEnv)->GetMethodID(pEnv, PowerManagerCls, "isScreenOn", "()Z");

  		if ((*pEnv)->CallBooleanMethod(pEnv, powerManager, isScreenOnMID)) {
  			(*pEnv)->SetIntField(pEnv, pObj, screenOnID, 1);
  			__android_log_print(ANDROID_LOG_INFO, "Morphone", "Set screenOn = 1 @2. %d",(*pEnv)->GetIntField(pEnv, pObj, screenOnID));
  		}
  		else{
  			(*pEnv)->SetIntField(pEnv, pObj, screenOnID, 0);
  			__android_log_print(ANDROID_LOG_INFO, "Morphone", "Set screenOn = 0 @2. %d",(*pEnv)->GetIntField(pEnv, pObj, screenOnID));
  		}
  		if ((*pEnv)->ExceptionOccurred(pEnv)) {
  			(*pEnv)->SetIntField(pEnv, pObj, screenOnID, -1);
  			__android_log_print(ANDROID_LOG_INFO, "Morphone", "Set screenOn = -1. %d",(*pEnv)->GetIntField(pEnv, pObj, screenOnID));
  		}
  	}
}

void throwScreenSenseException(JNIEnv *pEnv, jstring eMsg) {
	jclass ScreenSenseExceptionCls = (*pEnv)->FindClass(pEnv, "org/morphone/sense/screen/ScreenSenseException");
	if (ScreenSenseExceptionCls!=NULL) 
		(*pEnv)->ThrowNew(pEnv, ScreenSenseExceptionCls, eMsg);
	(*pEnv)->DeleteLocalRef(pEnv, ScreenSenseExceptionCls);
}
