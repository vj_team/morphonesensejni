TOP_PATH := $(call my-dir)

LOCAL_PATH := $(TOP_PATH)
include $(CLEAR_VARS)
LOCAL_MODULE    := AudioSense
LOCAL_SRC_FILES := AudioSense.c
LOCAL_LDLIBS := -llog
LOCAL_CPP_FEATURES += exceptions
include $(BUILD_SHARED_LIBRARY)

LOCAL_PATH := $(TOP_PATH)
include $(CLEAR_VARS)
LOCAL_MODULE    := BatterySense
LOCAL_SRC_FILES := BatterySense.c
LOCAL_LDLIBS := -llog
LOCAL_CPP_FEATURES += exceptions
include $(BUILD_SHARED_LIBRARY)

LOCAL_PATH := $(TOP_PATH)
include $(CLEAR_VARS)
LOCAL_MODULE    := BluetoothSense
LOCAL_SRC_FILES := BluetoothSense.c
LOCAL_LDLIBS := -llog
LOCAL_CPP_FEATURES += exceptions
include $(BUILD_SHARED_LIBRARY)

LOCAL_PATH := $(TOP_PATH)
include $(CLEAR_VARS)
LOCAL_MODULE    := BluetoothSenseReceiver
LOCAL_SRC_FILES := BluetoothSenseReceiver.c
LOCAL_LDLIBS := -llog
LOCAL_CPP_FEATURES += exceptions
include $(BUILD_SHARED_LIBRARY)

LOCAL_PATH := $(TOP_PATH)
include $(CLEAR_VARS)
LOCAL_MODULE    := CpuSenseCached
LOCAL_SRC_FILES := CpuSenseCached.c
LOCAL_LDLIBS := -llog
LOCAL_CPP_FEATURES += exceptions
include $(BUILD_SHARED_LIBRARY)

LOCAL_PATH := $(TOP_PATH)
include $(CLEAR_VARS)
LOCAL_MODULE    := CpuSensePolling
LOCAL_SRC_FILES := CpuSensePolling.c
LOCAL_LDLIBS := -llog
LOCAL_CPP_FEATURES += exceptions
include $(BUILD_SHARED_LIBRARY)

LOCAL_PATH := $(TOP_PATH)
include $(CLEAR_VARS)
LOCAL_MODULE    := DeviceSense
LOCAL_SRC_FILES := DeviceSense.c
LOCAL_LDLIBS := -llog
LOCAL_CPP_FEATURES += exceptions
include $(BUILD_SHARED_LIBRARY)

LOCAL_PATH := $(TOP_PATH)
include $(CLEAR_VARS)
LOCAL_MODULE    := HwSensors
LOCAL_SRC_FILES := AccelerometerSense.c LightSense.c
LOCAL_LDLIBS := -llog
LOCAL_CPP_FEATURES += exceptions
LOCAL_CFLAGS -= fno-builtin-memset
include $(BUILD_SHARED_LIBRARY)

LOCAL_PATH := $(TOP_PATH)
include $(CLEAR_VARS)
LOCAL_MODULE    := LocationSense
LOCAL_SRC_FILES := LocationSense.c
LOCAL_LDLIBS := -llog
LOCAL_CPP_FEATURES += exceptions
include $(BUILD_SHARED_LIBRARY)

LOCAL_PATH := $(TOP_PATH)
include $(CLEAR_VARS)
LOCAL_MODULE    := MemorySense
LOCAL_SRC_FILES := MemorySense.c
LOCAL_LDLIBS := -llog
LOCAL_CPP_FEATURES += exceptions
include $(BUILD_SHARED_LIBRARY)

LOCAL_PATH := $(TOP_PATH)
include $(CLEAR_VARS)
LOCAL_MODULE    := MobileSense
LOCAL_SRC_FILES := MobileSense.c
LOCAL_LDLIBS := -llog
LOCAL_CPP_FEATURES += exceptions
include $(BUILD_SHARED_LIBRARY)

LOCAL_PATH := $(TOP_PATH)
include $(CLEAR_VARS)
LOCAL_MODULE    := ScreenSense
LOCAL_SRC_FILES := ScreenSense.c
LOCAL_LDLIBS := -llog
LOCAL_CPP_FEATURES += exceptions
include $(BUILD_SHARED_LIBRARY)

LOCAL_PATH := $(TOP_PATH)
include $(CLEAR_VARS)
LOCAL_MODULE    := ScreenSenseReceiver
LOCAL_SRC_FILES := ScreenSenseReceiver.c
LOCAL_LDLIBS := -llog
LOCAL_CPP_FEATURES += exceptions
include $(BUILD_SHARED_LIBRARY)

LOCAL_PATH := $(TOP_PATH)
include $(CLEAR_VARS)
LOCAL_MODULE    := WifiSense
LOCAL_SRC_FILES := WifiSense.c
LOCAL_LDLIBS := -llog
LOCAL_CPP_FEATURES += exceptions
include $(BUILD_SHARED_LIBRARY)

LOCAL_PATH := $(TOP_PATH)
include $(CLEAR_VARS)
LOCAL_MODULE    := WifiSenseReceiver
LOCAL_SRC_FILES := WifiSenseReceiver.c
LOCAL_LDLIBS := -llog
LOCAL_CPP_FEATURES += exceptions
include $(BUILD_SHARED_LIBRARY)