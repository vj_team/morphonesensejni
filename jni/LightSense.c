#include <jni.h>
#include <stdio.h>
#include <android/log.h>

#include <AccelerometerSense.h>

JNIEXPORT jdouble JNICALL Java_org_morphone_sense_hw_1sensors_LightSense_getAmbientLightNative
  (JNIEnv *pEnv, jobject pObj) {
  	jclass AccelerometerSenseCls = (*pEnv)->GetObjectClass(pEnv, pObj);
  	jfieldID sensorEventID = (*pEnv)->GetFieldID(pEnv, AccelerometerSenseCls, "sensorEvent", "Landroid/hardware/SensorEvent;");
  	jobject sensorEvent = (*pEnv)->GetObjectField(pEnv, pObj, sensorEventID);
    jclass SensorEventCls = (*pEnv)->GetObjectClass(pEnv, sensorEvent);
  	jfieldID valuesID = (*pEnv)->GetFieldID(pEnv, SensorEventCls, "values", "[F");
  	jobject values = (*pEnv)->GetObjectField(pEnv, sensorEvent, valuesID);
  	jfloatArray* fValueArray = (jfloatArray*)(&values);
  	jsize valuesLen = (*pEnv)->GetArrayLength(pEnv, *fValueArray);
  	float* value = (*pEnv)->GetFloatArrayElements(pEnv, *fValueArray, 0);
    if((*pEnv)->ExceptionOccurred(pEnv)) {
      return -1;
    } else {
      return value[0];    
    }
}