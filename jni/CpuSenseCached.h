/* DO NOT EDIT THIS FILE - it is machine generated */
#include <jni.h>
/* Header for class org_morphone_sense_cpu_CpuSenseCached */

#ifndef _Included_org_morphone_sense_cpu_CpuSenseCached
#define _Included_org_morphone_sense_cpu_CpuSenseCached
#ifdef __cplusplus
extern "C" {
#endif
/*
 * Class:     org_morphone_sense_cpu_CpuSenseCached
 * Method:    getNumberOfCPUsNative
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_org_morphone_sense_cpu_CpuSenseCached_getNumberOfCPUsNative
  (JNIEnv *, jobject);

/*
 * Class:     org_morphone_sense_cpu_CpuSenseCached
 * Method:    getCpuCurrentFrequencyNative
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_morphone_sense_cpu_CpuSenseCached_getCpuCurrentFrequencyNative
  (JNIEnv *, jobject, jint);

/*
 * Class:     org_morphone_sense_cpu_CpuSenseCached
 * Method:    getCpuMaxScalingNative
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_morphone_sense_cpu_CpuSenseCached_getCpuMaxScalingNative
  (JNIEnv *, jobject, jint);

/*
 * Class:     org_morphone_sense_cpu_CpuSenseCached
 * Method:    getCpuMinScalingNative
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_morphone_sense_cpu_CpuSenseCached_getCpuMinScalingNative
  (JNIEnv *, jobject, jint);

/*
 * Class:     org_morphone_sense_cpu_CpuSenseCached
 * Method:    getCpuMaxFrequencyNative
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_morphone_sense_cpu_CpuSenseCached_getCpuMaxFrequencyNative
  (JNIEnv *, jobject, jint);

/*
 * Class:     org_morphone_sense_cpu_CpuSenseCached
 * Method:    getCpuMinFrequencyNative
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_morphone_sense_cpu_CpuSenseCached_getCpuMinFrequencyNative
  (JNIEnv *, jobject, jint);

/*
 * Class:     org_morphone_sense_cpu_CpuSenseCached
 * Method:    getCpuGovernorNative
 * Signature: (I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_org_morphone_sense_cpu_CpuSenseCached_getCpuGovernorNative
  (JNIEnv *, jobject, jint);

/*
 * Class:     org_morphone_sense_cpu_CpuSenseCached
 * Method:    getCpuUsageNative
 * Signature: (I)F
 */
JNIEXPORT jfloat JNICALL Java_org_morphone_sense_cpu_CpuSenseCached_getCpuUsageNative
  (JNIEnv *, jobject, jint);

void throwCPUSenseException(JNIEnv *, jstring);
char * getValueString (JNIEnv *, char *);
int getValue(JNIEnv *, char *);
#ifdef __cplusplus
}
#endif
#endif
