#include <jni.h>
#include <stdio.h>
#include <android/log.h>

#include <utilities.h>
#include <WifiSense.h>

JNIEXPORT jboolean JNICALL Java_org_morphone_sense_wifi_WifiSense_isConnectedNative
  (JNIEnv *pEnv, jobject pObj) {
	jclass WifiSenseCls = (*pEnv)->GetObjectClass(pEnv, pObj);
	jfieldID wifiManagerID = (*pEnv)->GetFieldID(pEnv, WifiSenseCls, "wifiManager", "Landroid/net/wifi/WifiManager;");
	jobject wifiManager = (*pEnv)->GetObjectField(pEnv, pObj, wifiManagerID);
	jclass WifiManagerCls = (*pEnv)->GetObjectClass(pEnv, wifiManager);
	jmethodID getConnectionInfoMID = (*pEnv)->GetMethodID(pEnv, WifiManagerCls, "getConnectionInfo", "()Landroid/net/wifi/WifiInfo;");
	jobject wifiInfo = (*pEnv)->CallObjectMethod(pEnv, wifiManager, getConnectionInfoMID);
	jfieldID wifiInfoID = (*pEnv)->GetFieldID(pEnv, WifiSenseCls, "wifiInfo", "Landroid/net/wifi/WifiInfo;");
	(*pEnv)->SetObjectField(pEnv, pObj, wifiInfoID, wifiInfo);

	jclass WifiInfoCls = (*pEnv)->GetObjectClass(pEnv, wifiInfo);
	jmethodID getSupplicantStateMID = (*pEnv)->GetMethodID(pEnv, WifiInfoCls, "getSupplicantState", "()Landroid/net/wifi/SupplicantState;");
	jobject supplicantState = (*pEnv)->CallObjectMethod(pEnv, wifiInfo, getSupplicantStateMID);
	jfieldID supplicantStateID = (*pEnv)->GetFieldID(pEnv, WifiSenseCls, "supplicantState", "Landroid/net/wifi/SupplicantState;");
	(*pEnv)->SetObjectField(pEnv, pObj, supplicantStateID, supplicantState);

	jstring COMPLETED = "COMPLETED", ASSOCIATED = "ASSOCIATED", INACTIVE = "INACTIVE";

	jclass SupplicantStateCls = (*pEnv)->GetObjectClass(pEnv, supplicantState);
	jmethodID toStringMID = (*pEnv)->GetMethodID(pEnv, SupplicantStateCls, "toString", "()Ljava/lang/String;");
	jstring supplicantStateStr = (jstring)(*pEnv)->CallObjectMethod(pEnv, supplicantState, toStringMID);
	if(strcmp(supplicantStateStr, COMPLETED) || strcmp(supplicantStateStr, ASSOCIATED) || strcmp(supplicantStateStr, INACTIVE))
		return 1;
	else
		return 0;
}

JNIEXPORT jboolean JNICALL Java_org_morphone_sense_wifi_WifiSense_isActiveNative
  (JNIEnv *pEnv, jobject pObj) {
	jclass WifiSenseCls = (*pEnv)->GetObjectClass(pEnv, pObj);
	jfieldID wifiManagerID = (*pEnv)->GetFieldID(pEnv, WifiSenseCls, "wifiManager", "Landroid/net/wifi/WifiManager;");
	jobject wifiManager = (*pEnv)->GetObjectField(pEnv, pObj, wifiManagerID);
	jclass WifiManagerCls = (*pEnv)->GetObjectClass(pEnv, wifiManager);
	jmethodID isWifiEnabledMID = (*pEnv)->GetMethodID(pEnv, WifiManagerCls, "isWifiEnabled", "()Z");
	return (*pEnv)->CallBooleanMethod(pEnv, wifiManager, isWifiEnabledMID);
}

JNIEXPORT jint JNICALL Java_org_morphone_sense_wifi_WifiSense_getNetworkIdNative
  (JNIEnv *pEnv, jobject pObj) {
	int networkId = -1;

	jclass WifiSenseCls = (*pEnv)->GetObjectClass(pEnv, pObj);
	jfieldID wifiInfoID = (*pEnv)->GetFieldID(pEnv, WifiSenseCls, "wifiInfo", "Landroid/net/wifi/WifiInfo;");
	jobject wifiInfo = (*pEnv)->GetObjectField(pEnv, pObj, wifiInfoID);
	jclass WifiInfoCls = (*pEnv)->GetObjectClass(pEnv, wifiInfo);
	jmethodID getNetworkIdMID = (*pEnv)->GetMethodID(pEnv, WifiInfoCls, "getNetworkId", "()I");
	networkId = (*pEnv)->CallIntMethod(pEnv, wifiInfo, getNetworkIdMID);
	if (networkId>=0)
		return networkId;
}

JNIEXPORT jint JNICALL Java_org_morphone_sense_wifi_WifiSense_getSignalStrengthNative
  (JNIEnv *pEnv, jobject pObj) {
	jclass WifiSenseCls = (*pEnv)->GetObjectClass(pEnv, pObj);
	jfieldID wifiInfoID = (*pEnv)->GetFieldID(pEnv, WifiSenseCls, "wifiInfo", "Landroid/net/wifi/WifiInfo;");
	jobject wifiInfo = (*pEnv)->GetObjectField(pEnv, pObj, wifiInfoID);
	jclass WifiInfoCls = (*pEnv)->GetObjectClass(pEnv, wifiInfo);
	jmethodID getRssiMID = (*pEnv)->GetMethodID(pEnv, WifiInfoCls, "getRssi", "()I");
	return (*pEnv)->CallIntMethod(pEnv, wifiInfo, getRssiMID);
}

JNIEXPORT jint JNICALL Java_org_morphone_sense_wifi_WifiSense_getLinkSpeedNative
  (JNIEnv *pEnv, jobject pObj) {
	jclass WifiSenseCls = (*pEnv)->GetObjectClass(pEnv, pObj);
	jfieldID wifiInfoID = (*pEnv)->GetFieldID(pEnv, WifiSenseCls, "wifiInfo", "Landroid/net/wifi/WifiInfo;");
	jobject wifiInfo = (*pEnv)->GetObjectField(pEnv, pObj, wifiInfoID);
	jclass WifiInfoCls = (*pEnv)->GetObjectClass(pEnv, wifiInfo);
	jmethodID getLinkSpeedMID = (*pEnv)->GetMethodID(pEnv, WifiInfoCls, "getLinkSpeed", "()I");
	return (*pEnv)->CallIntMethod(pEnv, wifiInfo, getLinkSpeedMID);
}

JNIEXPORT jint JNICALL Java_org_morphone_sense_wifi_WifiSense_getIPAddressNative
(JNIEnv *pEnv, jobject pObj) {
	jclass WifiSenseCls = (*pEnv)->GetObjectClass(pEnv, pObj);
	jfieldID wifiInfoID = (*pEnv)->GetFieldID(pEnv, WifiSenseCls, "wifiInfo", "Landroid/net/wifi/WifiInfo;");
	jobject wifiInfo = (*pEnv)->GetObjectField(pEnv, pObj, wifiInfoID);
	jclass WifiInfoCls = (*pEnv)->GetObjectClass(pEnv, wifiInfo);
	jmethodID getIPAddressMID = (*pEnv)->GetMethodID(pEnv, WifiInfoCls, "getIpAddress", "()I");
	return (*pEnv)->CallIntMethod(pEnv, wifiInfo, getIPAddressMID);
}

JNIEXPORT jlong JNICALL Java_org_morphone_sense_wifi_WifiSense_getTxBytesNative
  (JNIEnv *pEnv, jobject pObj) {
	jclass TrafficStatsCls = (*pEnv)->FindClass(pEnv, "android/net/TrafficStats");
	jfieldID UNSUPPORTEDID = (*pEnv)->GetStaticFieldID(pEnv, TrafficStatsCls, "UNSUPPORTED", "I");
	int UNSUPPORTED = (*pEnv)->GetStaticIntField(pEnv, TrafficStatsCls, UNSUPPORTEDID);

	long totalTxBytes = UNSUPPORTED;
	long mobileTxBytes = UNSUPPORTED;

	jmethodID getTotalTxBytesMID = (*pEnv)->GetStaticMethodID(pEnv, TrafficStatsCls, "getTotalTxBytes", "()J");
	jmethodID getMobileTxBytesMID = (*pEnv)->GetStaticMethodID(pEnv, TrafficStatsCls, "getMobileTxBytes", "()J");
	totalTxBytes = (*pEnv)->CallStaticLongMethod(pEnv, TrafficStatsCls, getTotalTxBytesMID);
	mobileTxBytes = (*pEnv)->CallStaticLongMethod(pEnv, TrafficStatsCls, getMobileTxBytesMID);

	if (!(totalTxBytes==UNSUPPORTED || mobileTxBytes==UNSUPPORTED))
		return (totalTxBytes-mobileTxBytes);
}

JNIEXPORT jlong JNICALL Java_org_morphone_sense_wifi_WifiSense_getRxBytesNative
  (JNIEnv *pEnv, jobject pObj) {
	jclass TrafficStatsCls = (*pEnv)->FindClass(pEnv, "android/net/TrafficStats");
	jfieldID UNSUPPORTEDID = (*pEnv)->GetStaticFieldID(pEnv, TrafficStatsCls, "UNSUPPORTED", "I");
	int UNSUPPORTED = (*pEnv)->GetStaticIntField(pEnv, TrafficStatsCls, UNSUPPORTEDID);

	long totalRxBytes = UNSUPPORTED;
	long mobileRxBytes = UNSUPPORTED;

	jmethodID getTotalRxBytesMID = (*pEnv)->GetStaticMethodID(pEnv, TrafficStatsCls, "getTotalRxBytes", "()J");
	jmethodID getMobileRxBytesMID = (*pEnv)->GetStaticMethodID(pEnv, TrafficStatsCls, "getMobileRxBytes", "()J");
	totalRxBytes = (*pEnv)->CallStaticLongMethod(pEnv, TrafficStatsCls, getTotalRxBytesMID);
	mobileRxBytes = (*pEnv)->CallStaticLongMethod(pEnv, TrafficStatsCls, getMobileRxBytesMID);

	if (!(totalRxBytes==UNSUPPORTED || mobileRxBytes==UNSUPPORTED))
		return (totalRxBytes-mobileRxBytes);
}

JNIEXPORT jstring JNICALL Java_org_morphone_sense_wifi_WifiSense_getMACAddressNative
  (JNIEnv *pEnv, jobject pObj) {
	jclass WifiSenseCls = (*pEnv)->GetObjectClass(pEnv, pObj);
	jfieldID macAddressID = (*pEnv)->GetFieldID(pEnv, WifiSenseCls, "macAddress", "Ljava/lang/String;");
	jstring macAddress = (jstring)(*pEnv)->GetObjectField(pEnv, pObj, macAddressID);
	if (macAddress!=NULL)
		return macAddress;
}
