/* DO NOT EDIT THIS FILE - it is machine generated */
#include <jni.h>
/* Header for class org_morphone_sense_memory_MemorySense */

#ifndef _Included_org_morphone_sense_memory_MemorySense
#define _Included_org_morphone_sense_memory_MemorySense
#ifdef __cplusplus
extern "C" {
#endif
/*
 * Class:     org_morphone_sense_memory_MemorySense
 * Method:    getUsedMemoryNative
 * Signature: ()J
 */
JNIEXPORT jlong JNICALL Java_org_morphone_sense_memory_MemorySense_getUsedMemoryNative
  (JNIEnv *, jobject);

/*
 * Class:     org_morphone_sense_memory_MemorySense
 * Method:    getTotalMemoryNative
 * Signature: ()J
 */
JNIEXPORT jlong JNICALL Java_org_morphone_sense_memory_MemorySense_getTotalMemoryNative
  (JNIEnv *, jobject);

void throwMemorySenseException(JNIEnv *, jstring);
#ifdef __cplusplus
}
#endif
#endif
