#include <jni.h>
#include <stdio.h>
#include <android/log.h>

#include <utilities.h>
#include <DeviceSense.h>

JNIEXPORT jint JNICALL Java_org_morphone_sense_device_DeviceSense_getSDKVersionNative
  (JNIEnv *pEnv, jobject pObj) {
	jclass DeviceSenseCls = (*pEnv)->GetObjectClass(pEnv, pObj);

	jclass BuildVersionCls = (*pEnv)->FindClass(pEnv, "android/os/Build$VERSION");

	jfieldID SDK_INTID = (*pEnv)->GetStaticFieldID(pEnv, BuildVersionCls, "SDK_INT", "I");
	int SDK_INT = (*pEnv)->GetStaticIntField(pEnv, BuildVersionCls, SDK_INTID);
	if((*pEnv)->ExceptionOccurred(pEnv)) {
		throwDeviceSenseException(pEnv, (*pEnv)->NewStringUTF(pEnv,"Error while getting SDKVersion"));
	}
	return SDK_INT;
}

JNIEXPORT jstring JNICALL Java_org_morphone_sense_device_DeviceSense_getBrandNative
  (JNIEnv *pEnv, jobject pObj) {
	jclass DeviceSenseCls = (*pEnv)->GetObjectClass(pEnv, pObj);

	jclass BuildCls = (*pEnv)->FindClass(pEnv, "android/os/Build");

	jfieldID BRANDID = (*pEnv)->GetStaticFieldID(pEnv, BuildCls, "BRAND", "Ljava/lang/String;");
	jstring BRAND = (jstring)(*pEnv)->GetStaticObjectField(pEnv, BuildCls, BRANDID);
	if((*pEnv)->ExceptionOccurred(pEnv)) {
		throwDeviceSenseException(pEnv, (*pEnv)->NewStringUTF(pEnv,"Error while getting brand"));
	}
	return BRAND;
}

JNIEXPORT jstring JNICALL Java_org_morphone_sense_device_DeviceSense_getModelNative
  (JNIEnv *pEnv, jobject pObj) {
	jclass DeviceSenseCls = (*pEnv)->GetObjectClass(pEnv, pObj);

	jclass BuildCls = (*pEnv)->FindClass(pEnv, "android/os/Build");

	jfieldID MODELID = (*pEnv)->GetStaticFieldID(pEnv, BuildCls, "MODEL", "Ljava/lang/String;");
	jstring MODEL = (jstring)(*pEnv)->GetStaticObjectField(pEnv, BuildCls, MODELID);
	if((*pEnv)->ExceptionOccurred(pEnv)) {
		throwDeviceSenseException(pEnv, (*pEnv)->NewStringUTF(pEnv,"Error while getting model"));
	}
	return MODEL;
}

JNIEXPORT jstring JNICALL Java_org_morphone_sense_device_DeviceSense_getKernelVersionNative
  (JNIEnv *pEnv, jobject pObj) {
	jclass SystemCls = (*pEnv)->FindClass(pEnv, "java/lang/System");
	jstring propertyName = (*pEnv)->NewStringUTF(pEnv, "os.version");

	jmethodID getPropertyMID = (*pEnv)->GetStaticMethodID(pEnv, SystemCls, "getProperty", "(Ljava/lang/String;)Ljava/lang/String;");
	jstring property = (*pEnv)->CallStaticObjectMethod(pEnv, SystemCls, getPropertyMID, propertyName);

	if (property!=NULL) {
		__android_log_print(ANDROID_LOG_INFO, "Morphone", "Got property");
		return property;
	}
	else {
		throwDeviceSenseException(pEnv, (*pEnv)->NewStringUTF(pEnv,"Error while getting systemProperty"));
		__android_log_print(ANDROID_LOG_INFO, "Morphone", "Error: Cannot get property");
		return "";
	}
}

JNIEXPORT jstring JNICALL Java_org_morphone_sense_device_DeviceSense_getArchNative
  (JNIEnv *pEnv, jobject pObj) {
	jclass SystemCls = (*pEnv)->FindClass(pEnv, "java/lang/System");
	jstring propertyName = (*pEnv)->NewStringUTF(pEnv, "os.arch");
	
	jmethodID getPropertyMID = (*pEnv)->GetStaticMethodID(pEnv, SystemCls, "getProperty", "(Ljava/lang/String;)Ljava/lang/String;");
	jstring property = (*pEnv)->CallStaticObjectMethod(pEnv, SystemCls, getPropertyMID, propertyName);

	if (property!=NULL) {
		__android_log_print(ANDROID_LOG_INFO, "Morphone", "Got property");
		return property;
	}
	else {
		throwDeviceSenseException(pEnv, (*pEnv)->NewStringUTF(pEnv,"Error while getting systemProperty"));
		__android_log_print(ANDROID_LOG_INFO, "Morphone", "Error: Cannot get property");
		return "";
	}
}

JNIEXPORT jstring JNICALL Java_org_morphone_sense_device_DeviceSense_getDeviceIdNative
  (JNIEnv *pEnv, jobject pObj) {
	jclass DeviceSenseCls = (*pEnv)->GetObjectClass(pEnv, pObj);

	jfieldID deviceIdID = (*pEnv)->GetFieldID(pEnv, DeviceSenseCls, "deviceId", "Ljava/lang/String;");
	jstring deviceId = (jstring)(*pEnv)->GetObjectField(pEnv, pObj, deviceIdID);
	if (deviceId!=NULL) {
		return deviceId;
	} else {
		if((*pEnv)->ExceptionOccurred(pEnv)) {
			throwDeviceSenseException(pEnv, (*pEnv)->NewStringUTF(pEnv,"Error while getting getMode"));
		}	
		return "";
	}
}

JNIEXPORT jstring JNICALL Java_org_morphone_sense_device_DeviceSense_getSystemPropertyNative
  (JNIEnv *pEnv, jobject pObj, jstring propertyName){
	jclass SystemCls = (*pEnv)->FindClass(pEnv, "java/lang/System");

	jmethodID getPropertyMID = (*pEnv)->GetStaticMethodID(pEnv, SystemCls, "getProperty", "(Ljava/lang/String;)Ljava/lang/String;");
	jstring property = (*pEnv)->CallStaticObjectMethod(pEnv, SystemCls, getPropertyMID, propertyName);

	if (property!=NULL) {
		__android_log_print(ANDROID_LOG_INFO, "Morphone", "Got property");
		return property;
	}
	else {
		if((*pEnv)->ExceptionOccurred(pEnv)) {
			throwDeviceSenseException(pEnv, (*pEnv)->NewStringUTF(pEnv,"Error while getting systemProperty"));
		}
		__android_log_print(ANDROID_LOG_INFO, "Morphone", "Error: Cannot get property");
		return "";
	}
}

void throwDeviceSenseException(JNIEnv *pEnv, jstring eMsg) {
	jclass DeviceSenseExceptionCls = (*pEnv)->FindClass(pEnv, "org/morphone/sense/device/DeviceSenseException");
	if (DeviceSenseExceptionCls!=NULL) 
		(*pEnv)->ThrowNew(pEnv, DeviceSenseExceptionCls, eMsg);
	(*pEnv)->DeleteLocalRef(pEnv, DeviceSenseExceptionCls);
}
