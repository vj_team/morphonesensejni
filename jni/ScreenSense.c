#include <jni.h>
#include <stdio.h>
#include <stdlib.h>
#include <android/log.h>

#include <utilities.h>
#include <ScreenSense.h>

JNIEXPORT jboolean JNICALL Java_org_morphone_sense_screen_ScreenSense_isScreenOnNative
  (JNIEnv *pEnv, jobject pObj) {
	jclass ScreenSenseCls = (*pEnv)->GetObjectClass(pEnv, pObj);

	jfieldID pmID = (*pEnv)->GetFieldID(pEnv, ScreenSenseCls, "pm", "Landroid/os/PowerManager;");
	jobject pm = (*pEnv)->GetObjectField(pEnv, pObj, pmID);
	jclass PowerManagerCls = (*pEnv)->GetObjectClass(pEnv, pm);
	jmethodID isScreenOnMID = (*pEnv)->GetMethodID(pEnv, PowerManagerCls, "isScreenOn", "()Z");
	return (*pEnv)->CallBooleanMethod(pEnv, pm, isScreenOnMID);
}

JNIEXPORT jfloat JNICALL Java_org_morphone_sense_screen_ScreenSense_getDisplayHeightNative
  (JNIEnv *pEnv, jobject pObj) {
	jclass ScreenSenseCls = (*pEnv)->GetObjectClass(pEnv, pObj);

	jfieldID displayID = (*pEnv)->GetFieldID(pEnv, ScreenSenseCls, "display", "Landroid/view/Display;");
	jobject display = (*pEnv)->GetObjectField(pEnv, pObj, displayID);
	jclass DisplayCls = (*pEnv)->GetObjectClass(pEnv, display);
	jmethodID getHeightMID = (*pEnv)->GetMethodID(pEnv, DisplayCls, "getHeight", "()I");
	return (*pEnv)->CallIntMethod(pEnv, display, getHeightMID);
}

JNIEXPORT jfloat JNICALL Java_org_morphone_sense_screen_ScreenSense_getDisplayWidthNative
  (JNIEnv *pEnv, jobject pObj) {
	jclass ScreenSenseCls = (*pEnv)->GetObjectClass(pEnv, pObj);

	jfieldID displayID = (*pEnv)->GetFieldID(pEnv, ScreenSenseCls, "display", "Landroid/view/Display;");
	jobject display = (*pEnv)->GetObjectField(pEnv, pObj, displayID);
	jclass DisplayCls = (*pEnv)->GetObjectClass(pEnv, display);
	jmethodID getWidthMID = (*pEnv)->GetMethodID(pEnv, DisplayCls, "getWidth", "()I");
	return (*pEnv)->CallIntMethod(pEnv, display, getWidthMID);
}

JNIEXPORT jfloat JNICALL Java_org_morphone_sense_screen_ScreenSense_getDisplayRefreshRateNative
  (JNIEnv *pEnv, jobject pObj) {
	jclass ScreenSenseCls = (*pEnv)->GetObjectClass(pEnv, pObj);

	jfieldID displayID = (*pEnv)->GetFieldID(pEnv, ScreenSenseCls, "display", "Landroid/view/Display;");
	jobject display = (*pEnv)->GetObjectField(pEnv, pObj, displayID);
	jclass DisplayCls = (*pEnv)->GetObjectClass(pEnv, display);
	jmethodID getRefreshRateMID = (*pEnv)->GetMethodID(pEnv, DisplayCls, "getRefreshRate", "()F");
	return (*pEnv)->CallFloatMethod(pEnv, display, getRefreshRateMID);
}

JNIEXPORT jfloat JNICALL Java_org_morphone_sense_screen_ScreenSense_getOrientationNative
(JNIEnv *pEnv, jobject pObj) {
	jclass ScreenSenseCls = (*pEnv)->GetObjectClass(pEnv, pObj);

	jfieldID displayID = (*pEnv)->GetFieldID(pEnv, ScreenSenseCls, "display", "Landroid/view/Display;");
	jobject display = (*pEnv)->GetObjectField(pEnv, pObj, displayID);
	jclass DisplayCls = (*pEnv)->GetObjectClass(pEnv, display);
	jmethodID getOrientationMID = (*pEnv)->GetMethodID(pEnv, DisplayCls, "getOrientation", "()I");
	return (*pEnv)->CallIntMethod(pEnv, display, getOrientationMID);
}

JNIEXPORT jint JNICALL Java_org_morphone_sense_screen_ScreenSense_getScreenBrightnessModeNative
  (JNIEnv *pEnv, jobject pObj) {
	jclass ScreenSenseCls = (*pEnv)->GetObjectClass(pEnv, pObj);

	jclass SystemCls = (*pEnv)->FindClass(pEnv, "android/provider/Settings$System");
	printClassName(pEnv, pObj, SystemCls);
	jmethodID getIntMID = (*pEnv)->GetStaticMethodID(pEnv, SystemCls, "getInt", "(Landroid/content/ContentResolver;Ljava/lang/String;)I");

	jfieldID contentResolverID = (*pEnv)->GetFieldID(pEnv, ScreenSenseCls, "contentResolver", "Landroid/content/ContentResolver;");
	jobject contentResolver = (*pEnv)->GetObjectField(pEnv, pObj, contentResolverID);
	jfieldID SCREEN_BRIGHTNESS_MODEID = (*pEnv)->GetStaticFieldID(pEnv, SystemCls, "SCREEN_BRIGHTNESS_MODE", "Ljava/lang/String;");
	jstring SCREEN_BRIGHTNESS_MODE = (*pEnv)->GetStaticObjectField(pEnv, SystemCls, SCREEN_BRIGHTNESS_MODEID);

	return (*pEnv)->CallStaticIntMethod(pEnv, SystemCls, getIntMID, contentResolver, SCREEN_BRIGHTNESS_MODE);
}

JNIEXPORT jint JNICALL Java_org_morphone_sense_screen_ScreenSense_getScreenBrightnessNative
  (JNIEnv *pEnv, jobject pObj) {
	jclass ScreenSenseCls = (*pEnv)->GetObjectClass(pEnv, pObj);

	jfieldID brightness_pathID = (*pEnv)->GetFieldID(pEnv, ScreenSenseCls, "brightness_path", "Ljava/lang/String;");
	jstring brightness_path = (jstring)(*pEnv)->GetObjectField(pEnv, pObj, brightness_pathID);

	jfieldID foundPatternID = (*pEnv)->GetFieldID(pEnv, ScreenSenseCls, "foundPattern", "Lorg/morphone/sense/screen/ScreenSense$FilePattern;");
	jobject foundPattern = (*pEnv)->GetObjectField(pEnv, pObj, foundPatternID);
	jclass FilePatternCls = (*pEnv)->GetObjectClass(pEnv, foundPattern);
	jfieldID brightnessFileID = (*pEnv)->GetFieldID(pEnv, FilePatternCls, "brightnessFile", "Lorg/morphone/sense/screen/ScreenSense$GenericFileFilter;");
	jobject brightnessFile = (*pEnv)->GetObjectField(pEnv, foundPattern, brightnessFileID);
	jclass GenericFileFilterCls = (*pEnv)->GetObjectClass(pEnv, brightnessFile);
	jfieldID fileNameID = (*pEnv)->GetFieldID(pEnv, GenericFileFilterCls, "fileName", "Ljava/lang/String;");
	jstring fileName = (jstring)(*pEnv)->GetObjectField(pEnv, brightnessFile, fileNameID);

	jbyte *bp = (*pEnv)->GetStringUTFChars(pEnv, brightness_path, NULL);
	jbyte *fn = (*pEnv)->GetStringUTFChars(pEnv, fileName, NULL);
	char *p = malloc(strlen(bp) + strlen(fn) + 1);
	strcpy(p, bp);
	strcat(p, fn);
	__android_log_print(ANDROID_LOG_INFO, "Morphone", "Path: %s", p);
	jstring path = (*pEnv)->NewStringUTF(pEnv, p);
	(*pEnv)->ReleaseStringUTFChars(pEnv, brightness_path, bp);
	(*pEnv)->ReleaseStringUTFChars(pEnv, fileName, fn);
	free(p);



	int sz = 8*24;
	char line[sz];
	FILE* fp = fopen(path, "r");

//	if (fp!=NULL) {
	if (fp == NULL){
		return -1;
	}
	else {
		fgets(line, 100, fp);
//		fflush(fp);

//	} else {
//		fclose(fp);
//	}
	int ret = atoi(line);
	fclose(fp);
	if (ret!=NULL)
		return ret;

	}
}

void throwScreenSenseException(JNIEnv *pEnv, jstring eMsg) {
	jclass ScreenSenseExceptionCls = (*pEnv)->FindClass(pEnv, "org/morphone/sense/screen/ScreenSenseException");
	if (ScreenSenseExceptionCls!=NULL) 
		(*pEnv)->ThrowNew(pEnv, ScreenSenseExceptionCls, eMsg);
	(*pEnv)->DeleteLocalRef(pEnv, ScreenSenseExceptionCls);
}
