package org.morphone.sense;

import java.util.Set;

import org.morphone.sense.audio.AudioSense;
import org.morphone.sense.audio.AudioSenseException;
import org.morphone.sense.battery.BatterySense;
import org.morphone.sense.battery.BatterySenseException;
import org.morphone.sense.bluetooth.BluetoothSense;
import org.morphone.sense.bluetooth.BluetoothSenseException;
import org.morphone.sense.bluetooth.BluetoothSenseReceiver;
import org.morphone.sense.cpu.CpuSenseCached;
import org.morphone.sense.cpu.CpuSenseException;
import org.morphone.sense.device.DeviceSense;
import org.morphone.sense.device.DeviceSenseException;
import org.morphone.sense.hw_sensors.AccelerometerSense;
import org.morphone.sense.location.LocationSense;
import org.morphone.sense.location.LocationSenseException;
import org.morphone.sense.memory.MemorySense;
import org.morphone.sense.memory.MemorySenseException;
import org.morphone.sense.mobile.MobileSense;
import org.morphone.sense.mobile.MobileSenseException;
import org.morphone.sense.screen.ScreenSense;
import org.morphone.sense.screen.ScreenSenseException;
import org.morphone.sense.screen.ScreenSenseReceiver;
import org.morphone.sense.wifi.WifiSense;
import org.morphone.sense.wifi.WifiSenseException;
import org.morphone.sense.wifi.WifiSenseReceiver;

import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.PowerManager;
import android.view.Menu;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

public class MainActivity extends Activity {
	
	private TextView tv;
	private Button bAndroid, bNative;
	private CheckBox cbNative;
	int selectedInfoId;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		tv = (TextView)findViewById(R.id.display_res);
		
//		addListenerOnButton();
		
//		callAudio();	// DONE
//		callBattery();		//DONE
//		callBluetooth();	//DONE
//		callBluetoothSenseReceiver();	//DONE
//		callDevice();
//		callWifi();	//DONE
//		callWifiSenseReceiver();	//DONE
//		callScreen();
//		callScreenSenseReceiver();
//		callMemory();
//		callHwSensors();
		
	}
	
	public void onButtonClicked(View v) {
		cbNative = (CheckBox) findViewById(R.id.cbNative);
		if (cbNative.isChecked()) 
			switch (v.getId()) {
			case R.id.bAudio:
				android.os.Debug.startMethodTracing("SenseAudioNative");
				callAudioNative();
				android.os.Debug.stopMethodTracing();
				break;
			case  R.id.bBluetooth:
				android.os.Debug.startMethodTracing("SenseBluetoothNative");
				callBluetoothNative();
				android.os.Debug.stopMethodTracing();
				break;
			case R.id.bDevice:
				android.os.Debug.startMethodTracing("SenseDeviceNative");
				callDeviceNative();
				android.os.Debug.stopMethodTracing();
				break;
			case R.id.bMemory:
				android.os.Debug.startMethodTracing("SenseMemoryNative");
				callMemoryNative();
				android.os.Debug.stopMethodTracing();
				break;
			case R.id.bScreen:
				android.os.Debug.startMethodTracing("SenseScreenNative");
				callScreenNative();
				android.os.Debug.stopMethodTracing();
				break;
			case R.id.bWifi:
				android.os.Debug.startMethodTracing("SenseWifiNative");
				callWifiNative();
				android.os.Debug.stopMethodTracing();
				break;
			case R.id.bBattery:
				android.os.Debug.startMethodTracing("SenseBatteryNative");
				callBatteryNative();
				android.os.Debug.stopMethodTracing();
				break;
//			case R.id.bCpu:
//				callMobileNative();
//				break;
			case R.id.bLocation:
				android.os.Debug.startMethodTracing("SenseLocationNative");
				callLocationNative();
				android.os.Debug.stopMethodTracing();
				break;
			}
		else 
			switch (v.getId()) {
			case R.id.bAudio:
				android.os.Debug.startMethodTracing("SenseAudio");
				callAudio();
				android.os.Debug.stopMethodTracing();
				break;
			case  R.id.bBluetooth:
				android.os.Debug.startMethodTracing("SenseBluetooth");
				callBluetooth();
				android.os.Debug.stopMethodTracing();
				break;
			case R.id.bDevice:
				android.os.Debug.startMethodTracing("SenseDevice");
				callDevice();
				android.os.Debug.stopMethodTracing();
				break;
			case R.id.bMemory:
				android.os.Debug.startMethodTracing("SenseMemory");
				callMemory();
				android.os.Debug.stopMethodTracing();
				break;
			case R.id.bScreen:
				android.os.Debug.startMethodTracing("SenseScreen");
				callScreen();
				android.os.Debug.stopMethodTracing();
				break;
			case R.id.bWifi:
				android.os.Debug.startMethodTracing("SenseWifi");
				callWifi();
				android.os.Debug.stopMethodTracing();
				break;
			case R.id.bBattery:
				android.os.Debug.startMethodTracing("SenseBattery");
				callBattery();
				android.os.Debug.stopMethodTracing();
				break;
//			case R.id.bCpu:
//				callMobile();
//				break;
			case R.id.bLocation:
				android.os.Debug.startMethodTracing("SenseLocation");
				callLocation();
				android.os.Debug.stopMethodTracing();
				break;
			}
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	public void callAudio() {
		AudioSense audioSense = new AudioSense(getApplicationContext());
		boolean bTemp;
		int iTemp;
		StringBuffer strBuf = new StringBuffer();
		
		try {
			bTemp = audioSense.isMusicActive();
			strBuf.append("\naudioSense.isMusicActive(").append(bTemp).append(")");
		} catch (AudioSenseException e) {
			e.printStackTrace();
		}
		
		try {
			iTemp = audioSense.getMusicVolume();
			strBuf.append("\naudioSense.getMusicVolume(").append(iTemp).append(")");
		} catch (AudioSenseException e) {
			e.printStackTrace();
		}
		
		try {
			iTemp = audioSense.getVoiceVolume();
			strBuf.append("\naudioSense.getVoiceVolume(").append(iTemp).append(")");
		} catch (AudioSenseException e) {
			e.printStackTrace();
		}
		
		try {
			iTemp = audioSense.getRingVolume();
			strBuf.append("\naudioSense.getRingVolume(").append(iTemp).append(")");
		} catch (AudioSenseException e) {
			e.printStackTrace();
		}
		
		try {
			bTemp = audioSense.isSpeakerOn();
			strBuf.append("\naudioSense.isSpeakerOn(").append(bTemp).append(")");
		} catch (AudioSenseException e) {
			e.printStackTrace();
		}
		
		try {
			iTemp = audioSense.getMode();
			strBuf.append("\naudioSense.getMode(").append(iTemp).append(")");
		} catch (AudioSenseException e) {
			e.printStackTrace();
		}
		
		tv.setText(strBuf.toString());
		
	}
	public void callAudioNative(){
		AudioSense audioSense = new AudioSense(getApplicationContext());
		boolean bTemp;
		int iTemp;
		StringBuffer strBuf = new StringBuffer();
		
		bTemp = audioSense.isMusicActiveNative();
		strBuf.append("\naudioSense.isMusicActiveNative(").append(bTemp).append(")");
		
		iTemp = audioSense.getMusicVolumeNative();
		strBuf.append("\naudioSense.getMusicVolumeNative(").append(iTemp).append(")");
		
		iTemp = audioSense.getVoiceVolumeNative();
		strBuf.append("\naudioSense.getVoiceVolumeNative(").append(iTemp).append(")");
		
		iTemp = audioSense.getRingVolumeNative();
		strBuf.append("\naudioSense.getRingVolumeNative(").append(iTemp).append(")");
		
		bTemp = audioSense.isSpeakerOnNative();
		strBuf.append("\naudioSense.isSpeakerOnNative(").append(bTemp).append(")");
		
		iTemp = audioSense.getModeNative();
		strBuf.append("\naudioSense.getModeNative(").append(iTemp).append(")");
		
		tv.setText(strBuf.toString());
	}
	
	public void callBattery(){
		BatterySense bs = new BatterySense(getApplicationContext());
		boolean bTemp;
		int iTemp;
		String strTemp;
		StringBuffer strBuf = new StringBuffer();
		
		try {
			bTemp = bs.isCharging();
			strBuf.append("\nbs.isCharging(").append(bTemp).append(")");
		} catch (BatterySenseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			iTemp = bs.getPercentage();
			strBuf.append("\nbs.getPercentage() = ").append(iTemp);
		} catch (BatterySenseException e) {
			e.printStackTrace();
		}
		
		try {
			iTemp = bs.getTemperature();
			strBuf.append("\nbs.getTemperature() = ").append(iTemp);
		} catch (BatterySenseException e) {
			e.printStackTrace();
		}
		
		try {
			iTemp = bs.getVoltage();
			strBuf.append("\nbs.getVoltage() = ").append(iTemp);
		} catch (BatterySenseException e) {
			e.printStackTrace();
		}
		
		try {
			strTemp = bs.getTechnology();
			strBuf.append("\nbs.getTechnology() = ").append(strTemp);
		} catch (BatterySenseException e) {
			e.printStackTrace();
		}
		
		try {
			iTemp = bs.getHealth();
			strBuf.append("\nbs.getHealth() = ").append(iTemp);
		} catch (BatterySenseException e) {
			e.printStackTrace();
		}
		
		bs.onReceive(getApplicationContext(), getIntent());
		
		tv.setText(strBuf.toString());
		
	}
	public void callBatteryNative(){
		BatterySense bs = new BatterySense(getApplicationContext());
		boolean bTemp;
		int iTemp;
		String strTemp;
		StringBuffer strBuf = new StringBuffer();
		
		bTemp = bs.isChargingNative();
		strBuf.append("\nbs.isChargingNative(").append(bTemp).append(")");
		
		iTemp = bs.getPercentageNative();
		strBuf.append("\nbs.getPercentageNative() = ").append(iTemp);
		
		iTemp = bs.getTemperatureNative();
		strBuf.append("\nbs.getTemperatureNative() = ").append(iTemp);
		
		iTemp = bs.getVoltageNative();
		strBuf.append("\nbs.getVoltageNative() = ").append(iTemp);
		strTemp = bs.getTechnologyNative();
		strBuf.append("\nbs.getTechnologyNative() = ").append(strTemp);
		
		iTemp = bs.getHealthNative();
		strBuf.append("\nbs.getHealthNative() = ").append(iTemp);
		
		bs.onReceiveNative(getApplicationContext(), getIntent());
		
		tv.setText(strBuf.toString());
		
	}

	public void callBluetooth() {
		BluetoothSense bs = new BluetoothSense();
		
		String strTemp;
		StringBuffer strBuf = new StringBuffer();
		
		int iTemp = -999;
		boolean bTemp;
		
		try {
			iTemp = bs.getState();
			strBuf.append("\nbs.getState() = ").append(iTemp);
		} catch (BluetoothSenseException e) {
			e.printStackTrace();
		}
		
		try {
			bTemp = bs.isActive();
			strBuf.append("\nbs.isActive() = ").append(bTemp);
		} catch (BluetoothSenseException e) {
			e.printStackTrace();
		}
		
		Set<BluetoothDevice> ret1 = null, ret2 = null;
		try {
			ret1 = bs.getBondedDevices();
		} catch (BluetoothSenseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ret2 = bs.getBondedDevicesNative();
		if (ret2.equals(ret1))
			bTemp = true;
		else
			bTemp = false;
		
		strBuf.append("\ngetBondedDevices() = getBondedDevicesNative() ? >>>>>").append(bTemp);
		
		tv.setText(strBuf.toString());
	}
	public void callBluetoothNative() {
		BluetoothSense bs = new BluetoothSense();
		
		String strTemp;
		StringBuffer strBuf = new StringBuffer();
		
		int iTemp = -999;
		boolean bTemp;
		
		iTemp = bs.getStateNative();
		strBuf.append("\nbs.getStateNative() = ").append(iTemp);
		
		bTemp = bs.isActiveNative();
		strBuf.append("\nbs.isActiveNative() = ").append(bTemp);
		
		Set<BluetoothDevice> ret1 = null, ret2 = null;
		try {
			ret1 = bs.getBondedDevices();
		} catch (BluetoothSenseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ret2 = bs.getBondedDevicesNative();
		if (ret2.equals(ret1))
			bTemp = true;
		else
			bTemp = false;
		
		strBuf.append("\ngetBondedDevices() = getBondedDevicesNative() ? >>>>>").append(bTemp);
		
		tv.setText(strBuf.toString());
	}

	public void callBluetoothSenseReceiver() {
		BluetoothSenseReceiver bs = new BluetoothSenseReceiver();
		
		String strTemp;
		StringBuffer strBuf = new StringBuffer();
		
		int iTemp = -999;
		boolean bTemp;
		
		try {
			iTemp = bs.getState();
			strBuf.append("\nbs.getState() = ").append(iTemp);
		} catch (BluetoothSenseException e) {
			e.printStackTrace();
		}
		
		try {
			bTemp = bs.isActive();
			strBuf.append("\nbs.isActive() = ").append(bTemp);
		} catch (BluetoothSenseException e) {
			e.printStackTrace();
		}
		
		Set<BluetoothDevice> ret1 = null, ret2 = null;
		try {
			ret1 = bs.getBondedDevices();
		} catch (BluetoothSenseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ret2 = bs.getBondedDevicesNative();
		if (ret2.equals(ret1))
			bTemp = true;
		else
			bTemp = false;
		
		strBuf.append("\ngetBondedDevices() = getBondedDevicesNative() ? >>>>>").append(bTemp);
		
		bs.onReceiveNative(getApplicationContext(), getIntent());
		
		tv.setText(strBuf.toString());
	}	
	public void callBluetoothSenseReceiverNative() {
		BluetoothSenseReceiver bs = new BluetoothSenseReceiver();
		
		String strTemp;
		StringBuffer strBuf = new StringBuffer();
		
		int iTemp = -999;
		boolean bTemp;
		
		iTemp = bs.getStateNative();
		strBuf.append("\nbs.getStateNative() = ").append(iTemp);
		
		bTemp = bs.isActiveNative();
		strBuf.append("\nbs.isActiveNative() = ").append(bTemp);
		
		Set<BluetoothDevice> ret1 = null, ret2 = null;
		try {
			ret1 = bs.getBondedDevices();
		} catch (BluetoothSenseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ret2 = bs.getBondedDevicesNative();
		if (ret2.equals(ret1))
			bTemp = true;
		else
			bTemp = false;
		
		strBuf.append("\ngetBondedDevices() = getBondedDevicesNative() ? >>>>>").append(bTemp);
		
		bs.onReceiveNative(getApplicationContext(), getIntent());
		
		tv.setText(strBuf.toString());
	}	

	public void callCpu() {
		CpuSenseCached cs = new CpuSenseCached();
		boolean bTemp;
		int iTemp;
		String strTemp;
		
		StringBuffer strBuf = new StringBuffer();
		
		try {
			strBuf.append("\ngetNumberOfCPUs() = ").append(cs.getNumberOfCPUs());
			int n = cs.getNumberOfCPUs();
			for (int i = 0; i < n; i++){
				strBuf.append("\ngetCpuCurrentFrequency(" + i + ") = ").append(cs.getCpuCurrentFrequency(i));
				strBuf.append("\ngetCpuMaxScaling(" + i + ") = ").append(cs.getCpuMaxScaling(i));
				strBuf.append("\ngetCpuMinScaling(" + i + ") = ").append(cs.getCpuMinScaling(i));
				strBuf.append("\ngetCpuMaxFrequency(" + i + ") = ").append(cs.getCpuMaxFrequency(i));
				strBuf.append("\ngetCpuMinFrequency(" + i + ") = ").append(cs.getCpuMinFrequency(i));
				strBuf.append("\ngetCpuGovernor(" + i + ") = ").append(cs.getCpuGovernor(i));
				strBuf.append("\ngetCpuUsage(" + i + ") = ").append(cs.getCpuUsage(i));
				
			}
		} catch (CpuSenseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		tv.setText(strBuf);
	}

	public void callCpuNative() {
		CpuSenseCached cs = new CpuSenseCached();
		boolean bTemp;
		int iTemp;
		String strTemp;
		
		StringBuffer strBuf = new StringBuffer();
		
		strBuf.append("\ngetNumberOfCPUs() = ").append(cs.getNumberOfCPUsNative());
		int n = cs.getNumberOfCPUsNative();
		for (int i = 0; i < n; i++){
//			strBuf.append("\ngetCpuCurrentFrequencyNative(" + i + ") = ").append(cs.getCpuCurrentFrequencyNative(i));
//			strBuf.append("\ngetCpuMaxScalingNative(" + i + ") = ").append(cs.getCpuMaxScalingNative(i));
//			strBuf.append("\ngetCpuMinScalingNative(" + i + ") = ").append(cs.getCpuMinScalingNative(i));
//			strBuf.append("\ngetCpuMaxFrequencyNative(" + i + ") = ").append(cs.getCpuMaxFrequencyNative(i));
//			strBuf.append("\ngetCpuMinFrequencyNative(" + i + ") = ").append(cs.getCpuMinFrequencyNative(i));
			strBuf.append("\ngetCpuGovernorNative(" + i + ") = ").append(cs.getCpuGovernorNative(i));
//			strBuf.append("\ngetCpuUsageNative(" + i + ") = ").append(cs.getCpuUsageNative(i));
			
		}
		tv.setText(strBuf);
	}
	
	public void callDevice() {
		DeviceSense ds = new DeviceSense(getApplicationContext());
		boolean bTemp;
		int iTemp;
		String strTemp;
		
		StringBuffer strBuf = new StringBuffer();
		
		try {
			strBuf.append("\ngetSDKVersion() = ").append(ds.getSDKVersion());
		} catch (DeviceSenseException e) {
			e.printStackTrace();
		}
		
		try {
			strBuf.append("\ngetBrand() = ").append(ds.getBrand());
		} catch (DeviceSenseException e) {
			e.printStackTrace();
		} 

		try {
			strBuf.append("\ngetModel() = ").append(ds.getModel());
		} catch (DeviceSenseException e) {
			e.printStackTrace();
		} 
		
		try {
			strBuf.append("\ngetKernelVersion() = ").append(ds.getKernelVersion());
		} catch (DeviceSenseException e) {
			e.printStackTrace();
		} 
		
		try {
			strBuf.append("\ngetDeviceId() = ").append(ds.getDeviceId());
		} catch (DeviceSenseException e) {
			e.printStackTrace();
		} 
		
		tv.setText(strBuf);
		strBuf.setLength(0);
	}
	public void callDeviceNative() {
		DeviceSense ds = new DeviceSense(getApplicationContext());
		boolean bTemp;
		int iTemp;
		String strTemp;
		
		StringBuffer strBuf = new StringBuffer();
		
		strBuf.append("\ngetSDKVersionNative() = ").append(ds.getSDKVersionNative());
		strBuf.append("\ngetBrandNative() = ").append(ds.getBrandNative());
		strBuf.append("\ngetModelNative() = ").append(ds.getModelNative());
		strBuf.append("\ngetKernelVersionNative() = ").append(ds.getKernelVersionNative());
		strBuf.append("\ngetArchNative() = ").append(ds.getArchNative());
//		strBuf.append("\ngetDeviceIdNative() = ").append(ds.getDeviceIdNative());
		
		tv.setText(strBuf);
		strBuf.setLength(0);
	}
	
	public void callWifi() {
		WifiSense ws = new WifiSense(
				(WifiManager)getApplicationContext().getSystemService(Context.WIFI_SERVICE), 
				(ConnectivityManager)getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE));
		StringBuffer strBuf = new StringBuffer();

		try {
			strBuf.append("\nisConnected() = ").append(ws.isConnected());
		} catch (WifiSenseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			strBuf.append("\nisActive() = ").append(ws.isActive());
		} catch (WifiSenseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			strBuf.append("\ngetNetworkId() = ").append(ws.getNetworkId());
		} catch (WifiSenseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			strBuf.append("\ngetSignalStrength() = ").append(ws.getSignalStrenght());
		} catch (WifiSenseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			strBuf.append("\ngetLinkSpeed() = ").append(ws.getLinkSpeed());
		} catch (WifiSenseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			strBuf.append("\ngetIPAddress() = ").append(ws.getIPAddress());
		} catch (WifiSenseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			strBuf.append("\ngetTxBytes() = ").append(ws.getTxBytes());
		} catch (WifiSenseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			strBuf.append("\ngetRxBytes() = ").append(ws.getRxBytes());
		} catch (WifiSenseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		try {
			strBuf.append("\ngetMACAddress() = ").append(ws.getMACAddress());
		} catch (WifiSenseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		tv.setText(strBuf);
		strBuf.setLength(0);
	}
	public void callWifiNative() {
		WifiSense ws = new WifiSense(
				(WifiManager)getApplicationContext().getSystemService(Context.WIFI_SERVICE), 
				(ConnectivityManager)getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE));
		StringBuffer strBuf = new StringBuffer();

		strBuf.append("\nisConnectedNative() = ").append(ws.isConnectedNative());
		strBuf.append("\nisActiveNative() = ").append(ws.isActiveNative());
		strBuf.append("\ngetNetworkIdNative() = ").append(ws.getNetworkIdNative());
		strBuf.append("\ngetSignalStrengthNative() = ").append(ws.getSignalStrengthNative());
		strBuf.append("\ngetLinkSpeedNative() = ").append(ws.getLinkSpeedNative());
		strBuf.append("\ngetIPAddressNative() = ").append(ws.getIPAddressNative());
		strBuf.append("\ngetTxBytesNative() = ").append(ws.getTxBytesNative());
		strBuf.append("\ngetRxBytesNative() = ").append(ws.getRxBytesNative());
		strBuf.append("\ngetMACAddressNative() = ").append(ws.getMACAddressNative());
		
			tv.setText(strBuf);
		strBuf.setLength(0);
	}	

	public void callWifiSenseReceiver() {
		WifiSenseReceiver wsr = new WifiSenseReceiver(getApplicationContext());
		StringBuffer strBuf = new StringBuffer();
		
		try {
			strBuf.append("\ngetNetworkId() = ").append(wsr.getNetworkId()).append("\ngetNetworkIdNative() = ").append(wsr.getNetworkIdNative());
		} catch (WifiSenseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			strBuf.append("\ngetSignalStrength() = ").append(wsr.getSignalStrenght()).append("\ngetSignalStrengthNative() = ").append(wsr.getSignalStrenghtNative());
		} catch (WifiSenseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			strBuf.append("\ngetLinkSpeed() = ").append(wsr.getLinkSpeed()).append("\ngetLinkSpeedNative() = ").append(wsr.getLinkSpeedNative());
		} catch (WifiSenseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			strBuf.append("\ngetIPAddress() = ").append(wsr.getIPAddress()).append("\ngetIPAddressNative() = ").append(wsr.getIPAddressNative());
		} catch (WifiSenseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			strBuf.append("\ngetTxBytes() = ").append(wsr.getTxBytes()).append("\ngetTxBytesNative() = ").append(wsr.getTxBytesNative());
		} catch (WifiSenseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			strBuf.append("\ngetRxBytes() = ").append(wsr.getRxBytes()).append("\ngetRxBytesNative() = ").append(wsr.getRxBytesNative());
		} catch (WifiSenseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			strBuf.append("\nisConnected() = ").append(wsr.isConnected());
		} catch (WifiSenseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		strBuf.append("\nisConnectedNative() = ").append(wsr.isConnectedNative());
		try {
			strBuf.append("\nisActive() = ").append(wsr.isActive());
		} catch (WifiSenseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		strBuf.append("\nisActiveNative() = ").append(wsr.isActiveNative());
		try {
			strBuf.append("\ngetMACAddress() = ").append(wsr.getMACAddress());
		} catch (WifiSenseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		strBuf.append("\ngetMACAddressNative() = ").append(wsr.getMACAddressNative());
		
		wsr.onReceive(getApplicationContext(), getIntent());
		wsr.onReceiveNative(getApplicationContext(), getIntent());
		
		
		tv.setText(strBuf);
		strBuf.setLength(0);
	}
	
	
	public void callScreen(){
		ScreenSense ss = new ScreenSense(
				(PowerManager) getSystemService(Context.POWER_SERVICE), 
				((WindowManager) getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay(), 
				getApplicationContext().getContentResolver());
		StringBuffer strBuf = new StringBuffer();
		
		try {
			strBuf.append("\nisScreenOn() = ").append(ss.isScreenOn());
		} catch (ScreenSenseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			strBuf.append("\ngetDisplayHeight() = ").append(ss.getDisplayHeight());
		} catch (ScreenSenseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			strBuf.append("\ngetDisplayWidth() = ").append(ss.getDisplayWidth());
		} catch (ScreenSenseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			strBuf.append("\ngetDisplayRefreshRate() = ").append(ss.getDisplayRefreshRate());
		} catch (ScreenSenseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			strBuf.append("\ngetOrientation() = ").append(ss.getOrientation());
		} catch (ScreenSenseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			strBuf.append("\ngetScreenBrightnessMode() = ").append(ss.getScreenBrightnessMode());
		} catch (ScreenSenseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			strBuf.append("\ngetScreenBrightness() = ").append(ss.getScreenBrightness());
		} catch (ScreenSenseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		tv.setText(strBuf);
		strBuf.setLength(0);
	}
	public void callScreenNative(){
		ScreenSense ss = new ScreenSense(
				(PowerManager) getSystemService(Context.POWER_SERVICE), 
				((WindowManager) getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay(), 
				getApplicationContext().getContentResolver());
		StringBuffer strBuf = new StringBuffer();
		
		strBuf.append("\nisScreenOnNative() = ").append(ss.isScreenOnNative());
		strBuf.append("\ngetDisplayHeightNative() = ").append(ss.getDisplayHeightNative());
		strBuf.append("\ngetDisplayWidthNative() = ").append(ss.getDisplayWidthNative());
		strBuf.append("\ngetDisplayRefreshRateNative() = ").append(ss.getDisplayRefreshRateNative());
		strBuf.append("\ngetOrientationNative() = ").append(ss.getOrientationNative());
		strBuf.append("\ngetScreenBrightnessModeNative() = ").append(ss.getScreenBrightnessModeNative());
		strBuf.append("\ngetScreenBrightnessNative() = ").append(ss.getScreenBrightnessNative());
		
		tv.setText(strBuf);
		strBuf.setLength(0);
	}
	
	public void callScreenSenseReceiver() {
		ScreenSenseReceiver ssr = new ScreenSenseReceiver(getApplicationContext());
		StringBuffer strBuf = new StringBuffer();
		
		try {
			strBuf.append("\nssr.getScreenBrightness() = ").append(ssr.getScreenBrightness());
		} catch (ScreenSenseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		strBuf.append("\nssr.getScreenBrightnessNative() = ").append(ssr.getScreenBrightnessNative());
		
		ssr.onReceiveNative(getApplicationContext(), getIntent());
		
		tv.setText(strBuf);
		strBuf.setLength(0);
	}

	
	public void callMobile() {
		MobileSense ms = new MobileSense(getApplicationContext());
		StringBuffer strBuf = new StringBuffer();
		
		try {
			strBuf.append("\ngetIMEI() = ").append(ms.getIMEI());
		} catch (MobileSenseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			strBuf.append("\nms.getCallState() = ").append(ms.getCallState());
		} catch (MobileSenseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			strBuf.append("\nms.getRxBytes() = ").append(ms.getRxBytes());
		} catch (MobileSenseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			strBuf.append("\nms.getTxBytes() = ").append(ms.getTxBytes());
		} catch (MobileSenseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			strBuf.append("\nms.getSignalStrenght() = ").append(ms.getSignalStrenght());
		} catch (MobileSenseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			strBuf.append("\nms.getDataState() = ").append(ms.getDataState());
		} catch (MobileSenseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			strBuf.append("\nms.getDataActivity() = ").append(ms.getDataActivity());
		} catch (MobileSenseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			strBuf.append("\nms.getNetworkType() = ").append(ms.getNetworkType());
		} catch (MobileSenseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		tv.setText(strBuf);
		strBuf.setLength(0);
	}	
	public void callMobileNative() {
		MobileSense ms = new MobileSense(getApplicationContext());
		StringBuffer strBuf = new StringBuffer();
		
			strBuf.append("\ngetIMEINative() = ").append(ms.getIMEINative());
			strBuf.append("\nms.getCallStateNative() = ").append(ms.getCallStateNative());
			strBuf.append("\nms.getRxBytesNative() = ").append(ms.getRxBytesNative());
			strBuf.append("\nms.getTxBytesNative() = ").append(ms.getTxBytesNative());
			strBuf.append("\nms.getSignalStrengthNative() = ").append(ms.getSignalStrengthNative());
			strBuf.append("\nms.getDataStateNative() = ").append(ms.getDataStateNative());
			strBuf.append("\nms.getDataActivityNative() = ").append(ms.getDataActivityNative());
			strBuf.append("\nms.getNetworkTypeNative() = ").append(ms.getNetworkTypeNative());
		
		tv.setText(strBuf);
		strBuf.setLength(0);
	}	
	public void callMemory() {
		MemorySense ms = new MemorySense(getApplicationContext());
		StringBuffer strBuf = new StringBuffer();
		
		try {
			strBuf.append("\ngetUsedMemory() = ").append(ms.getUsedMemory());
			strBuf.append("\ngetTotalMemory() = ").append(ms.getTotalMemory());
		} catch (MemorySenseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		tv.setText(strBuf);
		strBuf.setLength(0);
	}
	public void callMemoryNative() {
		MemorySense ms = new MemorySense(getApplicationContext());
		StringBuffer strBuf = new StringBuffer();
		
		strBuf.append("\ngetUsedMemoryNative() = ").append(ms.getUsedMemoryNative());
		strBuf.append("\ngetTotalMemoryNative() = ").append(ms.getTotalMemoryNative());
		
		tv.setText(strBuf);
		strBuf.setLength(0);
	}	
	public void callLocation() {
		LocationSense ls = new LocationSense(getApplicationContext());
		StringBuffer strBuf = new StringBuffer();
		
		try {
			strBuf.append("\nisGPSOn() = ").append(ls.isGPSOn());
			strBuf.append("\ngetCurrentProviderStatus() = ").append(ls.getCurrentProviderStatus());
		} catch (LocationSenseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		tv.setText(strBuf);
	}
	
	public void callLocationNative() {
		LocationSense ls = new LocationSense(getApplicationContext());
		StringBuffer strBuf = new StringBuffer();
		
			strBuf.append("\nisGPSOnNative() = ").append(ls.isGPSOnNative());
			strBuf.append("\ngetCurrentProviderStatusNative() = ").append(ls.getCurrentProviderStatusNative());
		tv.setText(strBuf);
	}
	public void callHW() {
		StringBuffer strBuf = new StringBuffer();
		AccelerometerSense as = new AccelerometerSense(getApplicationContext());
//		double x = as.getXAcceleration();
//		GenericSensorSense gss = new GenericSensorSense(getApplicationContext(), Sensor.TYPE_ACCELEROMETER);
//		double x = gss.sensorEvent.values[1];
		

//		System.out.println("X-Axis = " + x);
//		strBuf.append("\ngetXAcceleration() = ").append(as.getXAcceleration());
//		strBuf.append("\ngetXAccelerationNative() = ").append(as.getXAccelerationNative());
	}
}
