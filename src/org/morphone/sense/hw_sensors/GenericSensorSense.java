package org.morphone.sense.hw_sensors;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

public class GenericSensorSense {

	public SensorManager sensorManager;
	public Context context;
	public Sensor currentSensor;
	public int currentAccuracy;
	public SensorEvent sensorEvent;

//	double xAsis;
	
	public GenericSensorSense(Context cont, int sensor_type) {
		this.context = cont;
		sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
		sensorManager.registerListener(new MyListener(),
				sensorManager.getDefaultSensor(sensor_type),
				SensorManager.SENSOR_DELAY_NORMAL);
		}
	
	private class MyListener implements SensorEventListener{

		// TODO TeoF: check if the Override annotation has to be here
		// @Override
		public void onAccuracyChanged(Sensor sensor, int accuracy) {
			currentSensor = sensor;
			currentAccuracy = accuracy;
		}

		// TODO TeoF: check if the Override annotation has to be here
		// @Override
		public void onSensorChanged(SensorEvent event) {
			sensorEvent = event;
//			System.out.println("X = " + sensorEvent.values[0]);
//			System.out.println("Y = " + sensorEvent.values[1]);
//			System.out.println("Z = " + sensorEvent.values[2]);
//			System.out.println("X-Axis = " + xAsis);
		}
	}
}
